#
# Copyright (c) 2020-2023 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
# SPDX-License-Identifier: GPL-3.0-or-later
#

import pygame


# Local imports
from tempest.lib import logger
from tempest.lib import colors
from tempest import box
from tempest import disptext


class MenuItem:
    def __init__(self, name, value, active=True):
        self.name = name
        self.value = value
        self.active = active
        return


class Menu:
    def __init__(self, screen, font_name='freesans', font_size=20, bold=False,
                 fgcolor=colors.WHITE, bgcolor=colors.BLACK,
                 hlfgcolor=colors.BLACK, hlbgcolor=colors.WHITE,
                 dimcolor=colors.GRAY):

        self.items = []

        self.screen = screen
        self.font_name = font_name
        self.font_size = disptext.adj_font_size(screen, font_name, font_size,
                                                bold=bold)
        self.bold = bold
        self.fgcolor = fgcolor
        self.bgcolor = bgcolor
        self.hlfgcolor = hlfgcolor
        self.hlbgcolor = hlbgcolor
        self.dimcolor = dimcolor

        self.font = pygame.font.SysFont(self.font_name, self.font_size,
                                        bold=self.bold, italic=False)
        self.item_width = 0
        self.item_height = 0
        self.x_gap = 5
        self.y_gap = 5
        return


    def add(self, text, value, active=True):
        item = MenuItem(text, value, active)
        self.items.append(item)

        # Compute max size needed for a single item
        x, y = self.font.size(text)
        self.item_width = max(self.item_width, x)
        self.item_height = max(self.item_height, y)
        return


    def display(self, screen, menu_pos, mouse_pos):
        # Displays a menu
        # Returns a list of boxes containing all the menu items
        x, y = menu_pos

        width = self.item_width + self.x_gap * 2
        height = self.item_height + self.y_gap * 2
        menu_width = width
        menu_height = height * len(self.items)
        if x + menu_width > screen.get_width():
            x = screen.get_width() - menu_width
        if y + menu_height > screen.get_height():
            y = screen.get_height() - menu_height

        menu_box = box.Box((x, y), (x + menu_width, y + menu_height), 'menu')
        pygame.draw.rect(screen, self.bgcolor, menu_box.rect())
        menu_box.draw(screen, self.fgcolor)
        boxes = []
        for item in self.items:
            b = box.Box((x, y), (x + width, y + height), 'menu__' + item.name)
            boxes.append(b)

            if item.active is True:
                if b.contains(mouse_pos):
                    pygame.draw.rect(screen, self.hlbgcolor, b.rect())
                    txt = self.font.render(item.name, True, self.hlfgcolor,
                                           self.hlbgcolor)
                else:
                    txt = self.font.render(item.name, True, self.fgcolor)
            else:
                txt = self.font.render(item.name, True, self.dimcolor)

            screen.blit(txt, (x + self.x_gap, y + self.y_gap))
            y += height

        return boxes


    def popup(self, menu_pos, active=False):
        # Display a handle a popup menu
        # menu_pos is UL corner of desired location (may be moved if needed)
        # active is True, if items will hightlight when pointer is over them
        pos = menu_pos
        while True:
            boxes = self.display(self.screen, menu_pos, pos)
            pygame.display.update()

            event = pygame.event.wait()

            if event.type in (pygame.QUIT, pygame.VIDEORESIZE):
                # put this back and let the caller deal with it
                pygame.event.post(event)
                break

            if event.type == pygame.MOUSEBUTTONUP:
                pos = pygame.mouse.get_pos()
                bb = box.contains_list(pos, boxes)
                if bb is None:
                    logger.debug('No menu box')
                    break

                logger.debug('Click in box %s', bb.name)
                indx = boxes.index(bb)
                logger.debug('index=%d, item="%s", val=%s, active=%s', indx,
                             self.items[indx].name, self.items[indx].value,
                             self.items[indx].active)
                if self.items[indx].active is False:
                    active = False
                    pos = (-1, -1)
                    continue

                return self.items[indx].value

            if event.type == pygame.MOUSEBUTTONDOWN:
                logger.debug('Mouse button is now down')
                active = True

            if active:
                pos = pygame.mouse.get_pos()

        return None


if __name__ == "__main__":
    import screen
    import svg

    # Test code
    logger.init(None)

    pygame.font.init()
    screen = pygame.display.set_mode(screen.DEF_SIZE)
    pygame.event.set_blocked(pygame.ACTIVEEVENT)


    x = 20
    y = 20
    isize = 50
    mbox = box.Box((x, y), (x + isize, y + isize))
    micon = svg.load_file('tempest/icons/menu.svg', isize)
    mbox.draw(screen, colors.GREEN)

    m = Menu(screen, bold=True, hlfgcolor=colors.WHITE, hlbgcolor=colors.BLUE)
    m.add('Item 1', 1)
    m.add('Item 2', 'xyz')
    m.add('Item 3', 3)
    m.add('not active', 4, active=False)
    m.add('another choice', 0)
    m.add('Quit', -1)

    down_time = 0
    v = 0
    while True:
        screen.fill(colors.AQUA)
        screen.blit(micon, (x, y))
        #mbox.draw(screen, colors.GREEN)
        pygame.display.update()

        event = pygame.event.wait()
        if event.type == pygame.QUIT:
            break

        if event.type == pygame.MOUSEBUTTONUP:
            print('Got button UP')
            down_time = 0
            pos = pygame.mouse.get_pos()
            if mbox.contains(pos):
                print('inside menu icon')
                v = m.popup(pos, active=True)
                print('Value', v)

        if v == -1:
            break
