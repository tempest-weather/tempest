# Icons

This directory contains sets of icons.  One set is provided, but
others can be installed.  There are also some miscellaneous icons
here that are described below.

## Icon Sets

### Plain Weather

A family of icon sets that are setup to work with this program can be
found at <https://gitlab.com/tempest-weather/plain_weather>

See the information there for how to install and use them.

### Roll your own

You can add your own icon set here if you want.  To do so

* Create a new directory here.
* Place your icons (.svg format only) in it.
* Create a iconmap.json file that maps the icon names to filenames.
  Use `default/iconmap.json` as a template.
* Change the ICONS_DIR variable in config.py (top-level directory) to
the name of your new directory.

You might find running `python3 iconmap.py icons/<your-icon-dir>`
handy for checking out your iconmap.json file.


### default/

The `default` directory contains the default set of icons.
See [default/README.md](default/README.md) for details including licenses.


## Other icons


### tempest.png

This came from the same source as the icons in the default directory.
It is the dist/icons/black/png/64x64/chancerain.png file from that
same repository.  For licensing, see [default/README.md](default/README.md)


### menu.svg

Licensed according to the included ../LICENSE file.
