## Icons

Except as stated below, all icons in this directory are from
<https://github.com/manifestinteractive/weather-underground-icons.git>
from the directory `dist/icons/white/svg`.  They are dual licensed under
the MIT and GPL licenses


### wind.svg

[This icon](https://www.flaticon.com/free-icon/wind_959737) was made by
[Vitaly Gorbachev](https://www.flaticon.com/authors/vitaly-gorbachev) from
<https://www.flaticon.com/>.
It is "Free for personal and commercial purpose with attribution".
