#
# Control screen brightness
#
# Copyright (c) 2020,2023 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
# SPDX-License-Identifier: GPL-3.0-or-later
#

import datetime

# local imports
from tempest.lib import config
from tempest.lib import util
from tempest.lib import logger


def get_time_fr_num(n):
    # Return a datetime.time() for the time represented by the hhmm number
    # Do not use leading 0s.  e.g. 200 (for 02:00), 1600 for 16:00

    if n == 2400:
        n = 2359

    h = n // 100
    m = n % 100
    return datetime.time(h, m)


def is_time_in_range(t, time_tuple):
    # Returns True if time 't' is between the two times
    # in time_tuple (inclusive)
    start = get_time_fr_num(time_tuple[0])
    end = get_time_fr_num(time_tuple[1])

    if start <= t <= end:
        return True
    return False


def is_time_in_range_list(t, time_list):
    # Returns True if time 't' is between any of the time tuples in
    # the list time_list (inclusive)
    if not time_list:
        return False

    for times in time_list:
        if is_time_in_range(t, times):
            #logger.debug('in blank time %s', times)
            return True

    #logger.debug('not in any blank time')
    return False


class Brightness:
    # Note: A brightness delta of 10 is noticable at the low end, but
    # not so much at higher values

    # XXX This pathname may not be the same on all systems
    fname = "/sys/class/backlight/10-0045/brightness"
    fstate = config.STATE_DIR / 'brightness'
    min_start_value = 30
    blank_value = 10
    min_set_value = 10
    is_blanked = False


    def __init__(self):
        logger.debug('init')

        value = self.get_saved()

        if value is None:
            value = self.get() # Actual value

        # Make sure it isn't too low
        if value < self.min_start_value:
            value = self.min_start_value

        self.set(value)
        return


    def _save(self, value):
        # Save/change the current brightness value

        logger.debug('Setting brightness %d', value)
        try:
            with open(self.fname, "w") as fp:
                fp.write(str(value))
        except FileNotFoundError:
            logger.error("Unable to change screen brightness on this system")
            return True
        except Exception:
            logger.error("Can't change brightness to %d", value, exc_info=True)
            return False

        return True


    def get(self):
        # Fetch the current brightness value
        try:
            with open(self.fname) as fp:
                value = fp.read()
                value = int(value)
        except FileNotFoundError:
            logger.error("Unable to get screen brightness on this system")
            value = 100

        return value


    def inc(self, delta):
        # Increment the brightness value
        value = self.get() + delta
        return self.set(value)


    def dec(self, delta):
        # decrement the brightness value
        return self.inc(- delta)


    def set_session(self, value):
        # Set brightness for this session only.  i.e. do not remeber it
        return self._save(value)


    def set(self, value):
        # Set the brightness value (current session and save for future)
        if value > 255:
            value = 255
        if value < self.min_set_value:
            value = self.min_set_value

        rc = self.set_session(value)

        # Save this value for restarts
        with open(self.fstate, 'w') as fp:
            fp.write(str(value))

        return rc


    def get_saved(self):
        # Get the saved brightness value
        try:
            with open(self.fstate) as fp:
                value = fp.read()
                value = int(value)
            return value
        except FileNotFoundError:
            logger.error('%s, File not found', self.fstate)
            return None

        except Exception:
            logger.error("Can't read from %s", self.fstate, exc_info=True)
            return None

        return value


    def auto_adj(self, idle, alert):
        # idle is the idle time (time since last user input)
        # alert is true if there is an active alert
        # Return True if a change has been made

        now = util.get_time_of_day()
        now = now.replace(second=0, microsecond=0) # just HHMM
        if not is_time_in_range_list(now, config.BLANK_TIMES):
            idle = 0 # pretend active so we don't blank screen


        if alert:
            idle = 0 # pretend user active so we unblank if there is an alert

        if idle > config.BLANK_IDLE_TIME:
            if not self.is_blanked:
                logger.debug('blanking')
                self.set_session(self.blank_value)
                self.is_blanked = True
                return True
        elif idle < 1: # one sec seems ok
            if self.is_blanked:
                logger.debug('unblanking')
                value = self.get_saved()
                self.set_session(value)
                self.is_blanked = False
                return True

        return False
