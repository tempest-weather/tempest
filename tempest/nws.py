#
# National Weather Service API (US only)
#
# Copyright (c) 2020,2023 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
# SPDX-License-Identifier: GPL-3.0-or-later
#

# Official documentation
# ----------------------
# https://github.com/weather-gov/api
# https://www.weather.gov/documentation/services-web-api
# https://graphical.weather.gov/xml/xml_fields_icon_weather_conditions.php
# https://www.nws.noaa.gov/directives/
#  https://www.nws.noaa.gov/directives/010/010.php
#
# Notes and derived documentation on using the NSW API
# ----------------------------------------------------
#
# Users should use Last-Modified and set If-Modified-Since
# https://github.com/weather-gov/api/blob/master/gridpoints.md
# (urlcache should take care of this)
#
# Set User-Agent header to website and/or email
# User-Agent: (myweatherapp.com, contact@myweatherapp.com)
#
# https://codes.wmo.int/common/unit/m_s-1     meters/sec
# "unitCode": "unit:m_s-1",
#
# How to derive NWS zones
# Goto https://alerts.weather.gov/#us
# Following the 'Zone List' link for the desired state
# Find the zone best matching your area
#
# Alert info API
# https://api.weather.gov/...
#   alerts/active?area={state}
#   alerts/active/zone/{zoneId} e.g. from zone-list above
#   alerts/active/region/{region} (marine region)
#   alerts/active?status=actual&message_type=alert&point={LAT},{LON}"
# parameters:
#   message_type: alert, update, cancel
#   status: actual, exercise, system, test, draft
#   region_type: land, marine (incompat w/ area, point, region, zone)
#   point: lat,log (incompat w/ area, region, region_type, zone)
#   region: marine region code (incompat w/ area, point, region_type, zone)
#   area: state/marine area code (incompat w/ point, region, region_type, zone)
#   zone: Zone ID (forecast or county), (incompat w/ area, point, region, region_type) # noqa: E501
#   urgency: immediate, expected, future, past, unknown
#   severity: extreme, severe, moderate, minor, unknown
#   certainity: observed, likely, possible, unlikely, unknown
#
# Easy way to see sample alerts is to go to https://www.weather.gov/
# and look at what states have alerts and then goto
# https://api.weather.gov/alerts/active?area={state}
#
# alert response
# features[] is empty if no alerts
# features[]
#     .@id         "https://api.weather.gov/alerts/NWS-IDP-PROD-4139637-3506790"
#     .@type        "wx:Alert"
#     .id           "NWS-IDP-PROD-4139691-3506832"
#     .areaDesc     list of areas, might be long
#     .geocode      Some sort of ref to locations
#     .affectedZones[] Array of URLs to zones
#     .references[] reference other alerts
#     .sent         (datetime) often same as effective
#     .effective    (datetime) probably when issues
#     .onset        (datetime) when it goes into effect
#     .expires      (datetime) when it expires
#     .ends         (datetime) end time, might be 'null'
#                   sometimes .ends is better then .expires
#                   date format: "2020-04-07T02:19:00-07:00"
#     .status       "Actual"
#     .messageType  "Update", "Alert"
#     .category     "Met"
#     .severity     "Severe", "Moderate" (see parameters above)
#     .certainty    "Possible"          "Likely" (see parameters above)
#     .urgency      "Future"            "Expected" (see parameters above)
#     .event        "Flash Flood Watch" "Winder Storm Warning"
#                   "Special Weather Statement"
#     .sender
#     .senderName
#     .headline     Short summary with date/times
#     .description  (long desc, includes "\n")
#     .instruction  could be long, could be 'null'
#     .response     "Monitor"           "Prepare"
#                   "Execute"
#     .parameters
#
# forecast has daily info for
#    afternoon, tonight, monday, monday night,... sat night
#    7 days total
#         "detailedForecast": "A chance of rain showers. Mostly cloudy, with a low around 48. West wind around 6 mph. Chance of precipitation is 40%. New rainfall amounts less than a tenth of an inch possible.", # noqa: E501
#         "endTime": "2020-04-06T06:00:00-04:00",
#         "icon": "https://api.weather.gov/icons/land/night/rain_showers,40?size=medium", # noqa: E501
#         "isDaytime": false,
#         "name": "Tonight",
#         "number": 2,
#         "shortForecast": "Chance Rain Showers",
#         "startTime": "2020-04-05T18:00:00-04:00",
#         "temperature": 48,
#         "temperatureTrend": null,
#         "temperatureUnit": "F",
#         "windDirection": "W",
#         "windSpeed": "6 mph"  or "3 to 8 mph"
# "shortForecast" has lots of possibilities
#
# properties.periods[].temperature
#
# forecast hourly has
#         "number": 2,  (index in array)
#         "name": "",
#         "startTime": "2020-04-05T16:00:00-04:00",
#         "endTime": "2020-04-05T17:00:00-04:00",
#         "isDaytime": true,
#         "temperature": 66,
#         "temperatureUnit": "F",
#         "temperatureTrend": null,
#         "windSpeed": "7 mph",
#         "windDirection": "W",
#         "icon": "https://api.weather.gov/icons/land/day/sct?size=small",
#         "shortForecast": "Mostly Sunny",
#         "detailedForecast": ""
#
# forecast-grid is complicated
# value in SI units
# properties.temperature.values[].values.validTime
# properties.temperature.values[].values.value
# values for every hour
#   temp 9 days
#   precip 8 days (variable time periods: 1-13hr)
# no icon info
#
# Has the following fields
#  updateTime
#  validTimes
#  elevation
#  forecastOffice
#  gridId
#  gridX
#  gridY
#  temperature
#  dewpoint
#  maxTemperature
#  minTemperature
#  relativeHumidity
#  apparentTemperature
#  heatIndex
#  windChill
#  skyCover
#  windDirection
#  windSpeed
#  windGust
#  weather      (has some gaps, all can be null)
#     "coverage": "chance",
#     "weather": "rain_showers",
#     "intensity": "light",
#  hazards
#  probabilityOfPrecipitation
#  quantitativePrecipitation
#  iceAccumulation
#  snowfallAmount
#  snowLevel
#  ceilingHeight
#  visibility
#  transportWindSpeed
#  transportWindDirection
#  mixingHeight
#  hainesIndex
#    many of these below are completely empty (maybe just not relevant)
#  lightningActivityLevel
#  twentyFootWindSpeed
#  twentyFootWindDirection
#  waveHeight
#  wavePeriod
#  waveDirection
#  primarySwellHeight
#  primarySwellDirection
#  secondarySwellHeight
#  secondarySwellDirection
#  wavePeriod2
#  windWaveHeight
#  dispersionIndex
#  pressure
#  probabilityOfTropicalStormWinds
#  probabilityOfHurricaneWinds
#  potentialOf15mphWinds
#  potentialOf25mphWinds
#  potentialOf35mphWinds
#  potentialOf45mphWinds
#  potentialOf20mphWindGusts
#  potentialOf30mphWindGusts
#  potentialOf40mphWindGusts
#  potentialOf50mphWindGusts
#  potentialOf60mphWindGusts
#  grasslandFireDangerIndex
#  probabilityOfThunder
#  davisStabilityIndex
#  atmosphericDispersionIndex
#  lowVisibilityOccurrenceRiskIndex
#  stability
#  redFlagThreatIndex
#
#
# icon fields from forecast sample values
#   "https://api.weather.gov/icons/..."
#      "land/day/few?size=medium"
#      "land/night/bkn/rain_showers,40?size=medium"
#      "land/day/rain_showers,50/tsra,60?size=medium"
#      "land/night/tsra,60/tsra,50?size=medium"
#      "land/day/tsra_sct,40?size=medium"
#      "land/night/tsra_hi,30/tsra_hi,20?size=medium"
#      "land/day/rain_showers,50?size=medium"
#      "land/night/rain_showers,50/few?size=medium"
#      "land/day/sct?size=medium"
#      "land/night/sct?size=medium"
#      "land/day/rain_showers?size=medium"
#      "land/night/rain_showers,30?size=medium"
#      "land/day/rain_showers,50?size=medium"
#      "land/night/rain_showers,50/rain_showers,30?size=medium"
# small and large sizes are available
# The ",30" indicates % change of precip
# "land" might be "marine" in some places
#      land / [day|night] / icon ? arg
#      land / [day|night] / icon1 / icon2 ? arg
# icon may have a ,NNN suffix indication percent chance
#
# https://api.weather.gov/icons list of icons and descriptions (json)
#   icons.ICON.description    short textual desctiption
# where 'ICON' is the icon name above
# Seems that PNG is the only type available

################################################################################


from types import SimpleNamespace
import datetime
import urllib

import isodate
from dateutil import rrule
import pandas as pd
pd.options.mode.chained_assignment = 'raise'

# localimports
from tempest.lib import config
from tempest.lib import util
from tempest.lib import logger
from tempest.lib import units
from tempest import version
from tempest import jsonobj # noqa
from tempest import urlcache


def save_bad_result(result, src):
    fname = f'bad-data.{src}'
    logger.error('dumping bad data to %s for analysis', fname)
    with open(fname, 'wb') as fp:
        fp.write(result.content)


def get_value(prop):
    val = prop.value
    unit = prop.unitCode
    return units.convert(val, unit)


def grid_prop_to_pd(prop, column, accum=False):
    one_sec = datetime.timedelta(seconds=1)
    time_arr = []
    value_arr = []

    for v in prop.values:
        (date_str, duration_str) = v.validTime.split('/')
        dt = datetime.datetime.fromisoformat(date_str)
        delta = isodate.parse_duration(duration_str)

        for d in rrule.rrule(rrule.HOURLY,
                             dtstart=dt,
                             until=dt + delta - one_sec):
            dstr = d.strftime('%Y-%m-%dT%H:%M:%H%z')
            time_arr.append(dstr)
            if accum:
                # divide the amount evenly for the entire duration
                hrs = delta.total_seconds() / (60 * 60)
                value_arr.append(v.value / hrs)
            else:
                value_arr.append(v.value)

    #df = pd.DataFrame({'Time': time_arr, column: value_arr})
    index = pd.DatetimeIndex(time_arr)
    df = pd.DataFrame(data=value_arr, columns=[column], index=index)

    units.convert_df(df, column, prop.uom)
    return df


def grid_to_df(grid):
    # Create a pandas.DataFrame from the NFS forecast grid data
    # grid is the grid data in jsonObj format
    prop = grid.properties

    df_temp = grid_prop_to_pd(prop.temperature, 'Temp')
    df_dew = grid_prop_to_pd(prop.dewpoint, 'Dew')
    df_humid = grid_prop_to_pd(prop.relativeHumidity, 'Humid')
    df_feel = grid_prop_to_pd(prop.apparentTemperature, 'Feel')
    df_skycover = grid_prop_to_pd(prop.skyCover, 'Skycover')
    df_winddir = grid_prop_to_pd(prop.windDirection, 'WindDir')
    df_wind = grid_prop_to_pd(prop.windSpeed, 'Wind')
    df_gust = grid_prop_to_pd(prop.windGust, 'Gust')
    df_precip = grid_prop_to_pd(prop.probabilityOfPrecipitation, 'Precip')
    df_snowamt = grid_prop_to_pd(prop.snowfallAmount, 'Snow', accum=True)

    df = pd.concat([df_temp, df_feel, df_skycover, df_humid, df_dew,
                    df_wind, df_gust, df_winddir, df_precip, df_snowamt],
                   axis=1)

    return df


def parse_icon_url(url):
    # icon URLs looks somthing like...
    #   https://api.weather.gov/icons/land/night/few/tsra_hi,50?size=medium
    #   https://api.weather.gov/icons/land/day/tsra_hi,20?size=medium
    # see doc at top of file for more info
    #
    # returns (day_night, icon1, percent1, icon2, percent2)

    if url is None:
        return None, None, None, None, None

    purl = urllib.parse.urlparse(url)
    logger.debug('Parsed icon URL is %s', purl)

    # Remove prefixes
    path = purl.path
    path = path.replace('/icons/land/', '')
    path = path.replace('/icons/marine/', '')

    # Split into parts separated by
    path = path.split('/')
    logger.debug('icon split by / is %s', path)

    logger.debug('part1 %s', path[1])
    icon1, sep, percent1 = path[1].partition(',')
    if percent1 == '':
        percent1 = 0
    else:
        percent1 = int(percent1)

    if len(path) >= 3:
        logger.debug('part2 %s', path[2])
        icon2, sep, percent2 = path[2].partition(',')
        if percent2 == '':
            percent2 = 0
        else:
            percent2 = int(percent2)
    else:
        icon2 = icon1
        percent2 = percent1


    return path[0], icon1, percent1, icon2, percent2



def alert_starts(alert):
    if alert.onset:
        when = alert.onset
    elif alert.effective:
        when = alert.effective
    else:
        when = alert.sent

    return datetime.datetime.fromisoformat(when)


def alert_expires(alert):
    if alert.ends:
        when = alert.ends
    else:
        when = alert.expires

    return datetime.datetime.fromisoformat(when)


def is_json_newer(src, json):
    logger.debug('src.name=%s, time_field=%s', src.name, src.time_field)

    if src.time_field is None:
        logger.debug('No time_field, assuming this is newer')
        return True

    if src.jobj.obj is None:
        logger.debug('No current data, this must be newer')
        return True

    new_obj = jsonobj.JsonObj(json) # noqa: F841
    try:
        old_time = eval('src.jobj.obj.' + src.time_field)
        logger.debug('got old_time=%s', old_time)
        new_time = eval('new_obj.' + src.time_field)
        logger.debug('got new_time=%s', new_time)
    except (NameError, AttributeError) as e:
        logger.debug('Failed to get times, %s', e)
        return True # Assume it is newer

    now = util.now()
    old_dt = datetime.datetime.fromisoformat(old_time)
    new_dt = datetime.datetime.fromisoformat(new_time)

    if new_dt > now + datetime.timedelta(hours=4):
        logger.debug('new time is well into the future, assuming it is bad')
        return False

    if old_dt > now:
        if new_dt > now - datetime.timedelta(hours=4):
            logger.debug('old time is in the future and new time is not too '
                         'old, assuming it is better')
            return True

    if new_dt >= old_dt:
        logger.debug('new data is newer (or same time)')
        return True

    logger.error('New data is old')
    return False


class CurrentCond:
    # Class to hold current conditions
    # Not included are: dewpoint, visibility, min/max temp, percip, cloudLayers
    def __init__(self, station, prop):
        self.station = station

        if prop is None:
            self.ok = False
            self.time = util.now() - datetime.timedelta(hours=999)
            self.temperature = None
            self.wind_speed = None
            self.wind_dir = None
            self.wind_gust = None
            self.pressure = None
            self.humidity = None
            self.summary = 'Unknown'
            self.feels_like = None
            self.icon1 = None
            self.icon2 = None
            self.precipProbability1 = None
            self.precipProbability2 = None
            self.day_night = None
            return

        self.ok = True
        self.time = datetime.datetime.fromisoformat(prop.timestamp)
        self.temperature = get_value(prop.temperature)
        self.wind_speed = get_value(prop.windSpeed)
        self.wind_dir = get_value(prop.windDirection)
        self.wind_gust = get_value(prop.windGust)

        self.pressure = get_value(prop.seaLevelPressure)
        if self.pressure is None:
            # Do adjustment?  The difference between these two
            # values - even at high elevations - seems a lot lower then
            # expected
            self.pressure = get_value(prop.barometricPressure)

        self.humidity = get_value(prop.relativeHumidity)

        self.summary = prop.textDescription
        if self.summary is None:
            logger.debug('No summary')
            self.ok = False
            self.summary = 'Unknown'

        # heat index and wind chill will be None when they are not valid
        # https://github.com/weather-gov/api/issues/7#issuecomment-522157478
        wind_chill = get_value(prop.windChill)
        heat_index = get_value(prop.heatIndex)
        if wind_chill:
            self.feels_like = wind_chill
        elif heat_index:
            self.feels_like = heat_index
        else:
            self.feels_like = self.temperature

        day_night, \
            self.icon1, self.precipProbability1, \
            self.icon2, self.precipProbability2 = parse_icon_url(prop.icon)

        if self.temperature is None:
            logger.debug('No temp')
            self.ok = False

        if self.wind_speed is None:
            logger.debug('No wind')
            self.ok = False

        if self.icon1 is None:
            logger.debug('No icon')
            self.ok = False

        return

    def __repr__(self):
        s = ''
        if self.__class__.__module__ != "__main__":
            s = '{}.'.format(self.__class__.__module__)

        s += '{}(station={}, time={}, ok={})'. \
            format(self.__class__.__qualname__,
                   self.station, self.time, self.ok)
        return s



class Source:
    def __init__(self, name, path, station=False, url=None,
                 url_src=None, params=None, headers=None, time_field=None,
                 pretty=None, errmsg=None):
        if pretty is None:
            pretty = name.capitalize()

        self.name = name               # Name given to this source
        self.pretty = pretty           # Display name
        self.station = station         # True if this is a station
        self.url = url                 # URL to fetch (url_src ignored)
        self.url_src = url_src         # Lookup URL in this named source
        self.params = params           # Parameters added to URL
        self.headers = headers         # Headers added to HTTP request
        self.date = None               # When data was last updated
        self.errmsg = errmsg           # Error message to display if can't fetch
        self.time_field = time_field   # Field in json for when data was updated
        self.jobj = jsonobj.Json(path) # json object representing this data

        return


class Nws:
    nws_site = 'https://api.weather.gov'

    def __init__(self, lat=None, lon=None, dirname=config.CACHE_DIR):
        self.lat_lon = '{},{}'.format(lat, lon)
        self.dirname = dirname
        self.alerts = []
        self.sources = []
        self.current = CurrentCond(None, None)

        if lat and lon:
            self.have_location = True
        else:
            self.have_location = False

        if self.have_location:
            # This one must be before others that have 'url_src=points'
            src = Source(name='points',
                         errmsg='Are the LAT/LON in config.py correct?',
                         path='{}/nws-points.json'.format(dirname),
                         url='{}/points/{}'.format(self.nws_site, self.lat_lon))
            self.sources.append(src)

            src = Source(name='alerts',
                         errmsg='Are the LAT/LON in config.py correct?',
                         time_field='updated',
                         path='{}/nws-alerts.json'.format(dirname),
                         url='{}/alerts/active'.format(self.nws_site),
                         params={
                             'status': 'actual',
                             'point': self.lat_lon,
                         })
            self.sources.append(src)

            src = Source(name='forecast',
                         pretty='Daily forecast',
                         url_src='points',
                         headers={
                             'Pragma': 'akamai-x-cache-on, akamai-x-get-request-id'
                         },
                         time_field='properties.updateTime',
                         path='{}/nws-forecast.json'.format(dirname))
            self.sources.append(src)

            src = Source(name='forecast-grid',
                         pretty='Hourly forecast',
                         url_src='points',
                         time_field='properties.updateTime',
                         path='{}/nws-forecast-grid.json'.format(dirname))
            self.sources.append(src)

        for station in config.STATIONS:
            src = Source(name=station,
                         errmsg='Is STATIONS in config.py correct?',
                         pretty='Station {}'.format(station),
                         path='{}/nws-{}.json'.format(dirname, station),
                         time_field='properties.timestamp',
                         station=True,
                         url='{}/stations/{}/observations/latest'.format(
                             self.nws_site, station))
            self.sources.append(src)


        # NWS asks that contact info be placed in User-Agent header
        headers = {'User-Agent': version.proj_url}
        self.urlcache = urlcache.Url(self.dirname / 'web',
                                     headers=headers)
        return


    def get_url_from_source(self, src_name, this_name):
        logger.debug('Looking up URL for "%s" in source "%s"',
                     this_name, src_name)
        src = self.get_source_byname(src_name)
        if src is None:
            logger.debug('No source named %s', src_name)
            return None

        obj = src.jobj.obj
        if obj is None:
            logger.debug('Source has no jobj.obj')
            return None
        if this_name == 'forecast':
            return obj.properties.forecast
        if this_name == 'forecast-grid':
            return obj.properties.forecastGridData

        logger.critical('Unhandled name "%s"', this_name)
        raise ValueError


    def fetch(self):
        for src in self.sources:
            logger.debug('')
            logger.debug('Doing fetch for %s', src.name)
            url = src.url
            if url is None:
                url = self.get_url_from_source(src.url_src, src.name)
                if url is None:
                    logger.error('No URL found for %s', src.name)
                    continue

            result = self.urlcache.get(url, params=src.params,
                                       headers=src.headers)
            if result:
                try:
                    j = result.json()
                    if is_json_newer(src, j):
                        src.jobj.set(j)
                        src.jobj.save()
                    else:
                        logger.error('Keeping old data as it is actually newer')
                except ValueError:
                    logger.error('Unable to decode json')
                    save_bad_result(result, src.name)
            else:
                logger.error('Failed to download %s', url)
                if src.errmsg:
                    logger.error('%s', src.errmsg)

        self._update_times()
        self._update_alerts()
        self._update_current()
        return


    def invalidate(self):
        # Remove all downloaded data
        for src in self.sources:
            src.jobj.remove()
        return


    def load(self):
        for src in self.sources:
            src.jobj.load()

        self._update_times()
        self._update_alerts()
        self._update_current()
        return


    def _update_times(self):
        for src in self.sources:
            if src.time_field is None:
                if src.name == 'points':
                    # The points file doesn't have any timestamp, so just fake
                    # one up for yesterday.  It'll never be the cause for
                    # needing to fetch data
                    src.date = util.prev_day(util.now())
                else:
                    logger.error('No src.time_field specified for source %s',
                                 src.name)
                    raise AttributeError

                continue

            try:
                dstr = eval('src.jobj.obj.' + src.time_field)
                src.date = datetime.datetime.fromisoformat(dstr)
            except AttributeError as e:
                logger.error('Unable to update times for %s (no data?), %s',
                             src.name, e)

        return


    def _update_alerts(self):
        src = self.get_source_byname('alerts')
        if not src:
            return

        myzones = (self.get_forecast_zone(), self.get_county_zone())
        logger.debug('My zones: %s', myzones)

        self.alerts = []
        if src.jobj.obj is None:
            return

        for a in src.jobj.obj.features:
            logger.debug('Alert %s', a.id)
            if any(z in a.properties.affectedZones for z in myzones):
                self.alerts.append(a.properties)
            else:
                logger.debug('Alert not in my zone: %s',
                             a.properties.affectedZones)

        return


    def _update_current(self):
        stations = self.get_station_sources()
        curr_list = []

        for station in stations:
            logger.debug('Trying station %s', station.name)
            json = station.jobj.json
            if json is None:
                logger.debug('No json')
                continue

            curr = CurrentCond(station.name, station.jobj.obj.properties)
            curr_list.append(curr)

        # Return the most current station info
        # XXX consider favoring by station order unless much older
        logger.debug('before sort:%s', util.list_indent(curr_list))
        curr_list.sort(key=lambda c: c.time, reverse=True)
        curr_list.sort(key=lambda c: c.ok, reverse=True)
        logger.debug('after sort:%s', util.list_indent(curr_list))

        if len(curr_list):
            logger.debug('Using station %s', curr_list[0].station)
            self.current = curr_list[0]
        else:
            self.current = CurrentCond(None, None) # No station data
        return


    def get_source_byname(self, name):
        for src in self.sources:
            if src.name == name:
                return src
        return None


    def need(self):
        now = util.now()
        delta = datetime.timedelta(seconds=config.WEATHER_CHECK_INTERVAL)

        r = False
        for src in self.sources:
            logger.debug('%-20s: %s', src.name, src.date)
            if src.date is None:
                logger.debug('Need data source %s (missing)', src.name)
                r = True
            elif src.date + delta < now:
                logger.debug('Need data source %s (old)', src.name)
                r = True

        return r


    def need_reload(self):
        # Return true if any of the data sources need to be reloaded
        r = False

        for src in self.sources:
            if src.jobj.need_reload():
                logger.debug('Source %s needs to be reloaded', src.name)
                r = True

        return r


    def get_hourly_dataframe(self):
        grid = self.get_source_byname('forecast-grid')
        return grid_to_df(grid.jobj.obj)


    def get_daily(self, day_num):
        src = self.get_source_byname('forecast')
        try:
            obj = src.jobj.obj
            num_periods = len(obj.properties.periods)
        except AttributeError:
            logger.debug('forecast obj not set')
            num_periods = -1

        if day_num >= num_periods:
            logger.info('Daily forecast for day %d is not available yet')
            logger.debug('day=%s, max=%d', day_num, num_periods)
            return None

        p = obj.properties.periods[day_num]
        day = SimpleNamespace()
        day.name = p.name
        day.temperature = units.convert(p.temperature, p.temperatureUnit)
        day.time = datetime.datetime.fromisoformat(p.startTime)
        day.end_time = datetime.datetime.fromisoformat(p.endTime)

        day_night, \
            day.icon1, day.percent1, \
            day.icon2, day.percent2 = parse_icon_url(p.icon)

        if day_night == 'night':
            day.night = True
        else:
            day.night = False

        logger.debug('day %d is %s', day_num, day)
        return day


    def get_alert(self, num):
        try:
            return self.alerts[num]
        except IndexError:
            return None


    def get_num_alerts(self):
        return len(self.alerts)


    def get_station_sources(self):
        # Return a sublist of sources that are stations
        stations = []
        for src in self.sources:
            if src.station is True:
                stations.append(src)

        return stations


    def get_time(self):
        # Return the earliest time of all sources
        t = None
        for src in self.sources:
            if t is None:
                t = src.date
            else:
                t = min(t, src.date)

        return t


    def get_hourly_dataframe_time(self):
        src = self.get_source_byname('forecast-grid')
        if not src:
            return None
        return src.date


    def get_forecast_zone(self):
        src = self.get_source_byname('points')
        if not src:
            return None

        obj = src.jobj.obj
        if obj:
            return obj.properties.forecastZone
        return None


    def get_county_zone(self):
        src = self.get_source_byname('points')
        if not src:
            return None

        obj = src.jobj.obj
        if obj:
            return obj.properties.county
        return None


if __name__ == "__main__":
    # Test code
    logger.init(None)
    nws1 = Nws(config.LAT, config.LON, config.CACHE_DIR)

    nws1.fetch()

    nws = Nws(config.LAT, config.LON, config.CACHE_DIR)
    nws.load()
    day = nws.get_daily(0)

    for src in nws.sources:
        print('Name: {}'.format(src.name))
        print('Station: {}'.format(src.station))
        print('Date: {}'.format(src.date))
        print('URL: {}'.format(src.url))
        print('------')

    df = nws.get_hourly_dataframe()

    print('My dataframe head\n{}'.format(util.df_indent(df.head())))

    print('Daily for day 0\n  {}'.format(day))
