#
# Fetch radar images
#
# Copyright (c) 2020,2023 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
# SPDX-License-Identifier: GPL-3.0-or-later
#

import os
import time
import shutil

import pygame

# local imports
from tempest.lib import util
from tempest.lib import logger
from tempest.lib import config
from tempest import urlcache


def delete_old_data_files():
    # Check for radar files downloaded from old radar.weather.gov site
    radars = config.CACHE_DIR / 'radar'

    try:
        for f in os.listdir(radars):
            radar = os.path.join(radars, f)
            legend = os.path.join(radar, 'legend.gif')
            if os.path.exists(legend):
                logger.info('removing %s', radar)
                shutil.rmtree(radar)
    except FileNotFoundError:
        pass

    return


# For status page of all radars, see https://radar3pub.ncep.noaa.gov/
def radar_looks_down(fname):
    # Returns True is the radar is down, else False

    #XXXX revisit if we can figure out how to determine if it is down
    return False


class Radar:
    def __init__(self, site):
        self.site = site

        self.radar_dir = os.path.join(config.CACHE_DIR, 'radar', site)
        self.latest_image = '{}/{}_0.gif'.format(self.radar_dir, site)
        self.down_file = os.path.join(self.radar_dir, 'radar_down')

        self.urlcache = urlcache.Url(config.CACHE_DIR / 'web')
        return


    def fetch(self, rainsnow=False):
        # Downloads radar image data
        # XXX rainshow is not currently used
        base_url = 'https://radar.weather.gov/ridge/standard'

        # Make sure radar directory exists
        os.makedirs(self.radar_dir, exist_ok=True)

        ok = True
        #for num in range(10): # for when we support radar loops
        for num in range(1):
            fname = '{}_{}.gif'.format(self.site, num)
            url = '{}/{}'.format(base_url, fname)
            fpath = os.path.join(self.radar_dir, fname)
            tmppath = fpath + '.part'

            ok &= self.urlcache.download(url, tmppath)

            if ok is True:
                #XXXX try to determine if radar is down
                util.rm_f(self.down_file)

                os.rename(tmppath, fpath)

        return


    def down(self):
        # Return True if the radar is marked down
        if os.path.exists(self.down_file):
            logger.info('Radar %s is down', self.site)
            return True
        return False


    def get_image(self):
        # Load the radar image if found, else return None
        try:
            img = pygame.image.load(self.latest_image)
            logger.debug('read radar file %s', self.latest_image)
        except (pygame.error, FileNotFoundError):
            img = None

        return img


    def get_date(self):
        return util.date_from_file(self.latest_image)


    def need(self):
        logger.debug('radar_file is %s', self.latest_image)

        if not os.path.exists(self.latest_image):
            logger.info('No radar file for %s, need to fetch', self.site)
            return True

        age = time.time() - os.path.getmtime(self.latest_image)
        if age > config.RADAR_CHECK_INTERVAL:
            logger.info('radar data old (%d sec), need to fetch', age)
            return True

        return False


    def __repr__(self):
        s = ''
        if self.__class__.__module__ != "__main__":
            s = '{}.'.format(self.__class__.__module__)

        s += '{}(site={})'.format(self.__class__.__qualname__, self.site)
        return s


class Radars:
    # Like Radars, but handles a list of possible radar sites

    def __init__(self, radar_sites):
        delete_old_data_files()

        self.radar_list = []
        for site in radar_sites:
            self.radar_list.append(Radar(site))
        return

    def fetch(self, rainsnow=False):
        for r in self.radar_list:
            r.fetch(rainsnow)
        return

    def get_first_up(self):
        # Return the first radar that is up
        # If none are up, return the first with an image
        first = None
        for r in self.radar_list:
            img = r.get_image()
            if not first and img:
                first = r
            if not r.down():
                logger.debug('site %s not down, returning %s', r.site, r)
                return r

        logger.debug('at end returning %s', first)
        return first

    def get_image(self):
        r = self.get_first_up()
        if r:
            return r.get_image()
        return None

    def need(self):
        for r in self.radar_list:
            if r.need():
                return True
        return False


    def get_date(self):
        r = self.get_first_up()
        if r:
            return r.get_date()
        return None


if __name__ == "__main__":
    logger.init(None)

    for site in config.RADAR_SITES:
        r = Radar(site)
        r.fetch()
        print('Site', site)
        print('Radar down:', r.down())
        img = r.get_image()
        print('Image:', img)
