#!/usr/bin/env python3
#
# -*- coding: utf-8 -*-
#
# Tempest - A weather display program
#
# Copyright (c) 2014 Jim Kemp <kemp.jim@gmail.com>
# Copyright (c) 2017 Gene Liverman <gene@technicalissues.us>
# Copyright (c) 2019-2020,2023
#               Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
# SPDX-License-Identifier: GPL-3.0-or-later
#

"""
Fetches weather related information for displaying on a screen.
"""

###############################################################################


# initial standard imports
import os
os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide"
import pygame # noqa: I201

# initial local imports
from tempest.lib import logger
import tempest.version as version
##from tempest import __version__ as __version__ # XXXX not used yet

# Setup logging before anything else
logger.init(version.prog.lower())
version.info()

import tempest.screen as screen # noqa: I202
# Display splash screen right away before we do any more imports
# as some of them will take a long time on slow systems (i.e. Raspberry Pi)
screen.Screen.init()
screen.Screen.splash()

# More standard imports
import sys # noqa: I100
import time
import signal
import re
import datetime

# More local imports
import tempest.lib
from tempest.lib import util
from tempest.lib import config
from tempest.lib import colors
from tempest.lib import units
from tempest.lib import svg
import tempest
from tempest import disptext
from tempest import datafetch
from tempest import pidfile
from tempest import box
from tempest import brightness
from tempest import mouse
from tempest import nws
from tempest import radar
from tempest import water
from tempest import iconmap
from tempest import menu


def get_sunrise_sunset_strings():
    # Return strings for when the next sunrise and sunset will occur

    sr_suffix = 'today'
    ss_suffix = 'today'

    sr = util.sunrise(0) # today's sunrise
    if sr < util.now():
        sr = util.sunrise(1) # tomorrow's sunrise
        sr_suffix = 'tomorrow'

    ss = util.sunset(0) # today's sunset
    if ss < util.now():
        ss = util.sunset(1) # tomorrow's sunset
        ss_suffix = 'tomorrow'

    sr_str = sr.strftime("%H:%M {}").format(sr_suffix)
    ss_str = ss.strftime("%H:%M {}").format(ss_suffix)

    return sr_str, ss_str


def alert_to_color(alert):
    # Return the color that corresponds to the given alert severity
    if alert is None:
        return colors.WHITE

    # Full NWS color list is https://www.weather.gov/bro/mapcolors
    # but that is way to many and some may not work well on black.
    # Simplify the list to only a few colors to guage the severity
    # at a glance.
    # Using alert.event (e.g. "Severe Thunderstorm Watch") to
    # determine color as that seem to be better then looking
    # at the severity IMO

    if re.search(r'warning', alert.event, re.IGNORECASE):
        if re.search(r'tornado', alert.event, re.IGNORECASE):
            return colors.RED
        return colors.ORANGE
    if re.search(r'watch', alert.event, re.IGNORECASE):
        return colors.YELLOW
    if re.search(r'advisory', alert.event, re.IGNORECASE):
        return colors.GREEN

    return colors.AQUA


def alert_severity_num(alert):
    if alert is None:
        return 0

    if re.search(r'warning', alert.event, re.IGNORECASE):
        return 4
    if re.search(r'watch', alert.event, re.IGNORECASE):
        return 3
    if re.search(r'advisory', alert.event, re.IGNORECASE):
        return 2

    return 1


def most_severe(alert1, alert2):
    if alert_severity_num(alert1) > alert_severity_num(alert2):
        return alert1
    return alert2


def list_next_circular(cur, lst, reverse=False):
    # Returns the next item (after cur) in lst
    # If cur is not in list, returns first item

    logger.debug('cur=%s, lst=%s, rev=%s', cur, lst, reverse)
    if cur not in lst:
        logger.debug('Not in list, returning 1st item')
        return lst[0]

    if reverse:
        lst.reverse()

    # dup first at and to handle wrap
    lst.append(lst[0])

    i = lst.index(cur)
    new = lst[i + 1]
    logger.debug('returning %s', new)
    return new


def new_mode_from_keypress(mode, key):
    # Return a new mode based on the key that was pressed
    # may change global 'running'

    global running
    global my_disp

    # On 'q', quit the program.
    if key == pygame.K_q:
        running = False
        return mode # keep the same mode as we we are just going away

    if key == pygame.K_d:  # display weather
        mode = list_next_circular(mode, ['d1', 'd2', 'd3'])

    elif key == pygame.K_s:  # screen capture
        my_disp.screen_cap()

    elif key == pygame.K_i:  # display info
        mode = 'i'

    elif key == pygame.K_h:  # display hourly info
        mode = list_next_circular(mode, ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'])

    elif key == pygame.K_r:  # display radar
        mode = 'r'

    elif key == pygame.K_a:  # display alerts
        mode = 'a'

    elif key == pygame.K_LEFT: # prev page (same as swipe right)
        mode = next_page(mode, reverse=True)

    elif key == pygame.K_RIGHT: # next page (same as swipe left)
        mode = next_page(mode)

    return mode


###############################################################################
class MyDisplay:
    # Class for handling the display of the weather data
    # pylint: disable=too-many-instance-attributes

    daily_boxes = [None, None, None, None]
    border_width = 4

    def __init__(self):
        self.new_window_size()

        # Smaller text (as a percent of screen height)
        self.small_text_height = 0.075

        self.nws = nws.Nws(config.LAT, config.LON, config.CACHE_DIR)
        self.radars = radar.Radars(config.RADAR_SITES)
        self.water = water.Water()
        self.icons = iconmap.IconMap(config.ICON_SET)
        return

    def __del__(self):
        "Destructor to make sure pygame shuts down, etc."
        return


    def new_window_size(self):
        # Redo anything needed if the window size changes

        self.xmax, self.ymax = screen.Screen.size
        self.screen = screen.Screen.surface
        # Scale icon size for different screen sizes
        self.icon_size = 95 * min(self.xmax / screen.DEF_SIZE[0],
                                  self.ymax / screen.DEF_SIZE[1])
        self.define_boxes()

        self.menu = menu.Menu(self.screen, bold=True, hlfgcolor=colors.WHITE,
                              hlbgcolor=colors.BLUE)
        self.menu.add('First Daily Page', 'd1')
        self.menu.add('First Hourly Page', 'h1')
        self.menu.add('Radar', 'r')
        self.menu.add('Alerts', 'a')
        self.menu.add('Info', 'i')
        self.menu.add('Data Update Times', 'times')
        self.menu.add('About', 'about')
        self.menu.add('Quit', 'quit')
        return


    def load_data(self):
        # Load all weather data, return True is anything was loaded
        new = False

        if self.nws.need_reload():
            self.nws.load()
            new = True

        if self.water.need_reload():
            self.water.load()
            new = True

        # don't need to load radar as that is loaded during display
        return new


    def disp_icon(self, box, icon, percent1, percent2=None, icon_pos='center'):
        if icon_pos == 'center':
            icon_x = box.center_x() - icon.get_width() / 2
            icon_y = box.center_y() - icon.get_height() / 2

        if icon_pos == 'left-center':
            icon_x = box.center_x() - icon.get_width()
            icon_y = box.center_y() - icon.get_height() / 2

        if icon_pos == 'right-center':
            icon_x = box.center_x()
            icon_y = box.center_y() - icon.get_height() / 2

        if icon_pos == 'quarter-left':
            icon_x = box.left() + box.width() / 4
            icon_y = box.center_y() - icon.get_height() / 2

        logger.debug('icon position: %s/%s', icon_x, icon_y)
        self.screen.blit(icon, (int(icon_x), int(icon_y)))

        logger.debug('percent1=%s, percent2=%s', percent1, percent2)
        if not percent1 and not percent2:
            return

        if percent1 == percent2:
            percent2 = None

        font_size = 20

        if not percent2:
            # Display percent1 centered under icon
            x = icon_x + icon.get_width() / 2
            y = icon_y + icon.get_height()
            txt = units.pretty(percent1, '%')
            disptext.text(self.screen, txt, (x, y),
                          font_size=font_size, justify='center')
        else:
            # Display each % on bottom left and right respectively
            x1 = icon_x
            y = icon_y + icon.get_height()
            txt1 = units.pretty(percent1, '%')
            disptext.text(self.screen, txt1, (x1, y),
                          font_size=font_size, justify='center')

            x2 = icon_x + icon.get_width()
            y = icon_y + icon.get_height()
            txt2 = units.pretty(percent2, '%')
            disptext.text(self.screen, txt2, (x2, y),
                          font_size=font_size, justify='center')
        return


    def disp_daily_subwindow(self, data, day, win_num):
        # Display weather data (daily) in one of the bottom boxes
        boxes = (self.box_bot1, self.box_bot2, self.box_bot3, self.box_bot4)
        box = boxes[win_num]

        # space from one line to the next (about 8/win)
        y_gap = box.height() / 8
        y_start = box.top() + y_gap / 2

        if data is None:
            self.disp_no_data_box('data', box)
            self.daily_boxes[win_num] = None
            return

        # Save time that corresponds to each box
        self.daily_boxes[win_num] = data.time

        x = box.center_x()
        y = y_start + y_gap * 0 # Zero-th line

        disptext.autoscale_text(self.screen, day, (x, y),
                                min_font_size=10, font_size=24,
                                max_width=box.width() - self.border_width,
                                justify='center')

        now = util.now()
        if data.end_time + datetime.timedelta(hours=2) < now:
            # Add a note that the data is a bit old
            age = now - data.end_time
            y = y_start + y_gap * 1 # 1st line
            m = '{} ago'.format(util.time_delta_str(age))
            disptext.autoscale_text(self.screen, m, (x, y),
                                    min_font_size=10, font_size=20,
                                    max_width=box.width() - self.border_width,
                                    justify='center', color=colors.RED)


        # Weather icon
        size = self.icon_size
        if data.icon1 == data.icon2:
            icon1 = self.icons.load(data.icon1, size, night=data.night)
            self.disp_icon(box, icon1, data.percent1, percent2=data.percent2)
        else:
            icon1 = self.icons.load(data.icon1, size, night=data.night)
            icon2 = self.icons.load(data.icon2, size, night=data.night)
            self.disp_icon(box, icon1, data.percent1, icon_pos='left-center')
            self.disp_icon(box, icon2, data.percent2, icon_pos='right-center')

        # Temperature
        x = box.center_x()
        y = y_start + y_gap * 6 # Sixth line
        disptext.text(self.screen, units.pretty(data.temperature, 'Temp'),
                      (x, y), font_size=24, justify='center')
        return


    def disp_current_temp_subwindow(self, box, font_name, text_color):
        # Display the current temperature

        tsize = disptext.adj_font_size(self.screen, font_name, 91, bold=True)
        dsize = disptext.adj_font_size(self.screen, font_name, 43, bold=True)

        # space from one line to the next (about 8/win)
        y_gap = box.height() / 8
        y_start = box.top() + y_gap / 2
        y_pos = int(y_start + y_gap * 1)

        # Display the current temperature
        curr = self.nws.current
        # Don't specify units in call to pretty, so we can add our own
        # smaller degree symbol which looks better on the very large font
        temp = units.pretty(curr.temperature, 'Temp', units=False)
        font = pygame.font.SysFont(font_name, tsize, bold=True)
        txt = font.render(temp, True, text_color)

        # Use a smaller font for the degree symbol, otherwise it's way too big
        degfont = pygame.font.SysFont(font_name, dsize, bold=True)
        deg_txt = degfont.render(tempest.lib.UNICODE_DEGREE, True, text_color)

        txt_width = txt.get_width() + deg_txt.get_width()
        x = int(box.center_x() - (txt_width / 2))

        self.screen.blit(txt, (x, y_pos))
        self.screen.blit(deg_txt, (x + txt.get_width(), y_pos))
        return


    def disp_summary_subwindow(self, box):
        # Display current summary (text under current conditions icon/temp)
        curr = self.nws.current
        text = curr.summary
        # Some sample values
        #text = 'Possible Drizzle'
        #text = 'Light Rain and Fog/Mist'
        #text = "Thunderstorms and Heavy Rain and Fog/Mist"

        for size in range(24, 10, -1):
            dtext = disptext.TextBox(self.screen, box, font_size=size)
            dtext.line(text, line=-1)
            if dtext.get_xmax() > box.width():
                # If text is too long, try a smaller font till it fits
                # it it gets too small to read then just go with that
                logger.debug('font size %s is too big', size)
                logger.debug('box width=%s  xmax=%s',
                             box.width(), dtext.get_xmax())
                continue

            break

        dtext.display()
        return


    def disp_current_icon_subwindow(self, box):
        # Display current temp and condition summary

        # space from one line to the next (about 8/win)
        y_gap = box.height() / 8

        y_start = box.top() + y_gap / 2
        y_icon_offset = y_gap / 2
        logger.debug('y_start=%s, icon_offset=%s', y_start, y_icon_offset)

        curr = self.nws.current
        icon = self.icons.load(curr.icon1, self.icon_size)
        x = int(box.center_x() - icon.get_width() / 2)
        y = int(y_start + y_gap * 0 + y_icon_offset)
        self.screen.blit(icon, (x, y))
        return


    def disp_left_vert_text(self, box, text, fontname, color, justify='center'):
        text_height = 0.04
        gap = 5

        font = pygame.font.SysFont(fontname,
                                   int(self.ymax * text_height),
                                   bold=1)
        txt = font.render(text, True, color)
        txt = pygame.transform.rotate(txt, 90)

        # left and right justify is based on turning your head 90deg left
        if justify == 'center':
            y = int(box.center_y() - txt.get_height() / 2)
        elif justify == 'left':
            y = box.bottom() - txt.get_height() - gap
        elif justify == 'right':
            y = box.top() + gap

        self.screen.blit(txt, (box.left() + gap, y))
        return


    def disp_daily(self, first_day, last_day):
        # Display daily weather data (mode d1, d2, & d3)
        self.screen.fill(colors.BLACK)
        line_color = colors.WHITE
        text_color = colors.WHITE

        self.draw_screen_border(line_color, tempest.BOX_STYLE_DAILY)
        self.disp_time_date(tempest.DEF_FONT_NAME, text_color)
        self.disp_left_vert_text(self.box_mid1a, 'Currently',
                                 tempest.DEF_FONT_NAME, text_color)
        self.disp_current_icon_subwindow(self.box_mid1a)
        self.disp_current_temp_subwindow(self.box_mid1b,
                                         tempest.DEF_FONT_NAME, text_color)
        self.disp_summary_subwindow(self.box_mid1)

        curr = self.nws.current
        if not curr.station:
            self.disp_no_data('Current conditions', 'Check config file')
            return

        age = util.now() - curr.time
        txt = '{}, {} ago'.format(curr.station, util.time_delta_str(age))
        if age > datetime.timedelta(hours=2):
            color = colors.RED
        else:
            color = colors.WHITE

        ttime = disptext.TextBox(self.screen, self.box_mid1, font_size=18,
                                 color=color)
        ttime.line(txt)
        ttime.display()

        conds = {}
        conds.update({'Feels Like:': units.pretty(curr.feels_like, 'Temp')})

        wind_str = units.pretty_wind(curr.wind_speed, curr.wind_dir)
        if curr.wind_gust:
            wind_str += ' G{}'.format(units.pretty(curr.wind_gust, None, None))
        conds.update({'Wind:': wind_str})

        conds.update({'Humidity:': units.pretty(curr.humidity, '%')})
        conds.update({'Pressure:': units.pretty(curr.pressure, 'Pressure')})

        if config.WATER_SITE:
            s = units.pretty_water(self.water.temp, self.water.height)
            conds.update({'Water:': s})

        size = 24
        tconds = disptext.TextBox(self.screen, self.box_mid2, font_size=size)
        tconds.name_values(conds)
        tconds.display()

        # Display primary alert
        y = self.box_mid2.top() + tconds.get_ymax()
        x = self.box_mid2.left() + tconds.xgap
        alert = self.get_primary_alert()
        if alert is None:
            color = text_color
            txt = 'No alerts'
        else:
            color = alert_to_color(alert)
            txt = alert.event

        disptext.text(self.screen, txt, (x, y), font_size=size, color=color)

        # Display daily sub windows
        num = 0
        for day in range(first_day, last_day + 1):
            daily = self.nws.get_daily(day)
            if daily is None:
                self.disp_daily_subwindow(None, None, num)
            else:
                self.disp_daily_subwindow(daily, daily.name, num)
            num += 1

        # Update the display
        pygame.display.update()
        return


    def disp_hourly(self, day):
        # Display hourly weather graphs
        self.screen.fill(colors.BLACK)

        day -= 1
        graph_file = '{}/day_{}.png'.format(config.hourly_dir, day)

        try:
            img = pygame.image.load(graph_file)
        except pygame.error:
            self.disp_no_data('hourly data for day {}'.format(day))
            return

        offset = (4, 4)
        self.screen.blit(img, offset)
        # Draw border after image as some images may cover the menu icon
        self.draw_screen_border(colors.WHITE, tempest.BOX_STYLE_OUTLINE)
        pygame.display.update()
        return


    def disp_radar(self):
        # Display radar image
        self.screen.fill(colors.BLACK)  # black background
        self.draw_screen_border(colors.WHITE, tempest.BOX_STYLE_OUTLINE)

        img = self.radars.get_image()
        if not img:
            self.disp_no_data('radar')
            return

        # center image in screen
        width = img.get_width()
        height = img.get_height()
        x_off = screen.Screen.size[0] / 2 - width / 2
        y_off = screen.Screen.size[1] / 2 - height / 2
        offset = (int(x_off), int(y_off))
        logger.debug('using offset of %s', offset)

        self.screen.blit(img, offset)

        # Update the display
        pygame.display.update()
        return


    def disp_no_data_box(self, item, box=None, font_size=20):
        # Display a message indicating we don't have any data to display
        logger.debug('Displaying No %s message in box %s', item, box)
        dtext = disptext.TextBox(self.screen, box, xgap=10,
                                 font_size=font_size)
        txt = 'No {}'.format(item)
        dtext.line(txt, line=1, justify='left')
        dtext.display()
        return


    def disp_no_data(self, item, item2=None):
        # Display a message indicating we don't have any data to display
        text_color = colors.WHITE

        self.screen.fill(colors.BLACK)
        self.draw_screen_border(colors.WHITE, tempest.BOX_STYLE_TITLE)
        self.disp_time_date(tempest.DEF_FONT_NAME, text_color)

        dtext = disptext.TextBox(self.screen, self.box_main, xgap=20,
                                 font_size=36)
        txt = 'No {} available'.format(item)
        dtext.line(txt, line=1, justify='left')
        if item2:
            dtext.line(item2, line=2, justify='left')

        dtext.display()
        pygame.display.update()
        return


    def disp_title(self, title, font_name, color):
        logger.debug('displaying title: %s', title)
        x = self.box_top.center_x()
        y = self.box_top.center_y()
        disptext.text(self.screen, title, (x, y), color=color,
                      font_size=55, font_name=font_name,
                      justify='center', vjustify='center')
        return


    def disp_time_date(self, font_name, text_color):
        # Display the current date/time in the title box
        time_string = time.strftime("%a, %b %d  %H:%M", time.localtime())
        self.disp_title(time_string, font_name, text_color)
        return


    def define_boxes(self):
        # Screen looks like this
        #
        # BOX_STYLE_DAILY
        #  +-----------------------+h1   top line
        #  |          top          |
        #  +-----------+-----------+h2   bottom top box
        #  |mid1 |mid1 |    mid2   |
        #  |  a  |  b  |           |
        #  +---- +-----+-----+-----+h3   bottom middle box
        #  |     |     |     |     |
        #  |bot1 |bot2 |bot3 |bot4 |
        #  |     |     |     |     |
        #  +-----+-----+-----+-----+h4   bottom line
        #  v1    v2    v3    v4    v5
        #
        # BOX_STYLE_TITLE
        #  +-----------------------+h1   top line
        #  |          top          |
        #  +-----------------------+h2   bottom top box
        #  |                       |
        #  |                       |
        #  |         main          |
        #  |                       |
        #  |                       |
        #  |                       |
        #  +-----+-----+-----+-----+h4   bottom line
        #  v1    v2    v3    v4    v5
        #
        # BOX_STYLE_OUTLINE
        #  +-----------------------+h1   top line
        #  |                       |
        #  |                       |
        #  |                       |
        #  |                       |
        #  |         full          |
        #  |                       |
        #  |                       |
        #  |                       |
        #  +-----+-----+-----+-----+h4   bottom line
        #  v1    v2    v3    v4    v5
        #
        # BOX_STYLE_ALERTS
        #  +-----------------------+h1   top line
        #  |          top          |
        #  +-----------------------+h2   bottom top box
        #  |         alert1        |
        #  +-----------------------+a1
        #  |         alert2        |
        #  +-----------------------+a2
        #  |         alert3        |
        #  +-----------------------+a3
        #  |         alert4        |
        #  +-----------------------+a4
        #  |         alert5        |
        #  +-----+-----+-----+-----+h4   bottom line
        #  v1    v2    v3    v4    v5
        #
        #
        a1 = self.ymax * (0.15 + 0.17 * 1)
        a2 = self.ymax * (0.15 + 0.17 * 2)
        a3 = self.ymax * (0.15 + 0.17 * 3)
        a4 = self.ymax * (0.15 + 0.17 * 4)
        v1 = self.xmax * 0
        v2 = self.xmax * 0.25
        v3 = self.xmax * 0.5
        v4 = self.xmax * 0.75
        v5 = self.xmax * 1.0
        h1 = self.ymax * 0
        h2 = self.ymax * 0.15
        h3 = self.ymax * 0.5
        h4 = self.ymax * 1.0

        logger.debug('defining boxes')
        self.box_screen = box.Box((v1, h1), (v5, h4), 'screen')
        self.box_top = box.Box((v1, h1), (v5, h2), 'top')
        self.box_mid1 = box.Box((v1, h2), (v3, h3), 'mid1') # mid1a & mid1b
        self.box_mid1a = box.Box((v1, h2), (v2, h3), 'mid1a')
        self.box_mid1b = box.Box((v2, h2), (v3, h3), 'mid1b')
        self.box_mid2 = box.Box((v3, h2), (v5, h3), 'mid2')
        self.box_bot1 = box.Box((v1, h3), (v2, h4), 'bot1')
        self.box_bot2 = box.Box((v2, h3), (v3, h4), 'bot2')
        self.box_bot3 = box.Box((v3, h3), (v4, h4), 'bot3')
        self.box_bot4 = box.Box((v4, h3), (v5, h4), 'bot4')
        self.box_main = box.Box((v1, h2), (v5, h4), 'bot5')
        self.box_alert1 = box.Box((v1, h2), (v5, a1), 'alert1')
        self.box_alert2 = box.Box((v1, a1), (v5, a2), 'alert2')
        self.box_alert3 = box.Box((v1, a2), (v5, a3), 'alert3')
        self.box_alert4 = box.Box((v1, a3), (v5, a4), 'alert4')
        self.box_alert5 = box.Box((v1, a4), (v5, h4), 'alert5')

        # get position and icon for menu.  Keep the box square,
        # and in the upper right hand corner
        x = self.xmax * 0.96
        y = self.border_width
        size = self.xmax - x
        x -= self.border_width
        self.box_menu = box.Box((x, y), (x + size, y + size), 'menu')
        logger.debug('menu is at %s', self.box_menu)
        self.menu_icon = svg.load_file(iconmap.dir() / 'menu.svg',
                                       self.box_menu.width())
        return


    def get_alert_box(self, num):
        # Return the box corresponding to the number of the alert.
        # See BOX_STYLE_ALERTS
        if num == 1:
            return self.box_alert1
        if num == 2:
            return self.box_alert2
        if num == 3:
            return self.box_alert3
        if num == 4:
            return self.box_alert4
        if num == 5:
            return self.box_alert5
        return False


    def draw_screen_border(self, color, box_style, width=None):
        # See define_boxes for where lines should be drawn

        if width is None:
            width = self.border_width

        # Menu icon, not box around it
        self.screen.blit(self.menu_icon, self.box_menu.UL())

        if box_style in (tempest.BOX_STYLE_DAILY, tempest.BOX_STYLE_TITLE,
                         tempest.BOX_STYLE_ALERTS):
            self.box_top.draw(self.screen, colors.WHITE, width)

        if box_style == tempest.BOX_STYLE_DAILY:
            #self.box_mid1a.draw(self.screen, colors.WHITE, width)
            #self.box_mid1b.draw(self.screen, colors.WHITE, width)
            self.box_mid1.draw(self.screen, colors.WHITE, width)
            self.box_mid2.draw(self.screen, colors.WHITE, width)
            self.box_bot1.draw(self.screen, colors.WHITE, width)
            self.box_bot2.draw(self.screen, colors.WHITE, width)
            self.box_bot3.draw(self.screen, colors.WHITE, width)
            self.box_bot4.draw(self.screen, colors.WHITE, width)

        if box_style == tempest.BOX_STYLE_ALERTS:
            # XXX should we even draw these?  color dark gray?
            self.box_alert1.draw(self.screen, colors.WHITE, width)
            self.box_alert2.draw(self.screen, colors.WHITE, width)
            self.box_alert3.draw(self.screen, colors.WHITE, width)
            self.box_alert4.draw(self.screen, colors.WHITE, width)
            self.box_alert5.draw(self.screen, colors.WHITE, width)

        # Change border color if there is an alert
        alert = self.get_primary_alert()
        if alert is not None:
            logger.debug('Alert present, drawing alert box')
            color = alert_to_color(alert)

        # Draw boarder around edge of screen
        self.box_screen.draw(self.screen, color, width)
        return


    def box_from_position(self, pos, style):
        # Determine which box is at the give position

        # Always check for menu box first
        if self.box_menu.contains(pos):
            return self.box_menu

        # Note that order matters in the lists below when boxes overlap

        boxes = []
        if style == tempest.BOX_STYLE_ALERTS:
            boxes = [self.box_top, self.box_alert1, self.box_alert2,
                     self.box_alert3, self.box_alert4, self.box_alert5]

        if style == tempest.BOX_STYLE_DAILY:
            boxes = [self.box_top,
                     self.box_mid1, self.box_mid2,
                     self.box_bot1, self.box_bot2, self.box_bot3, self.box_bot4]

        return box.contains_list(pos, boxes)


    def handle_click(self, pos, mode):
        # Handle mouse clicks
        # Returns the possibly new mode
        # pylint: disable=too-many-branches
        logger.debug('Got mouse click at %s, mode is %s', pos, mode)

        style = tempest.BOX_STYLE_DAILY
        if mode == 'a':
            style = tempest.BOX_STYLE_ALERTS

        box = self.box_from_position(pos, style)
        if box is None:
            logger.debug('Click not in any box')
            return mode
        logger.debug('Click in box %s', box.name)

        if box == self.box_menu:
            v = self.menu.popup(pos, active=True)
            if v is not None:
                return v

        if box == self.box_top:
            # Click in title area (even if not visible) we go to main screen
            return 'd1'

        if mode == 'a':
            if box == self.box_alert1:
                return 'a1'
            if box == self.box_alert2:
                return 'a2'
            if box == self.box_alert3:
                return 'a3'
            if box == self.box_alert4:
                return 'a4'
            if box == self.box_alert5:
                return 'a5'

        if mode in ('a1', 'a2', 'a3', 'a4', 'a5'):
            if box != self.box_top:
                return 'a' # click almost anywhere, return to main alert screen

        if mode in ('d1', 'd2', 'd3'):
            if box == self.box_mid2:
                return 'a'

            if box == self.box_mid1:
                return 'r'

        if mode in ('d1', 'd2', 'd3'):
            # handle click in daily boxes
            hr_page = self.box_to_hourly_page(box)
            logger.debug('box %s maps to %s', box.name, hr_page)
            if hr_page:
                return hr_page

        if mode in ('r', 'i', 'times', 'about'):
            return 'd1' # return to main daily screen

        #XXX This tries to go back to the matching daily screen,
        #XXX but this doesn't always to the right thing
        #XXX i.e. we can get to h2 via both d1 and d2 in the evening
        #XXX so there is no way to select the right one all the time
        if mode in ('h1', 'h2'):
            return 'd1' # return to main daily screen

        if mode in ('h3', 'h4'):
            return 'd2' # return to second daily screen

        if mode in ('h5', 'h6'):
            return 'd3' # return to third daily screen

        return mode


    def disp_update_times(self):
        color = colors.WHITE

        self.screen.fill(colors.BLACK)
        self.draw_screen_border(color, tempest.BOX_STYLE_TITLE)
        self.disp_title('Data Source updates', tempest.DEF_FONT_NAME, color)

        dlist = disptext.TextBox(self.screen, self.box_main,
                                 font_size=28)

        items = {}
        for src in self.nws.sources:
            if src.name == 'points':
                # time is fake, no point is displaying it
                continue

            name = '{}:'.format(src.pretty)

            if src.date is None:
                t = '?'
            else:
                t = util.time_str_and_delta(src.date)

            items.update({name: t})

        dt = self.radars.get_date()
        if dt is None:
            t = None
        else:
            t = util.time_str_and_delta(dt)
        items.update({'Radar:': t})

        dt = self.water.get_date()
        if dt is None:
            t = None
        else:
            t = util.time_str_and_delta(dt)
        items.update({'Water data:': t})

        dlist.name_values(items)
        dlist.display()
        pygame.display.update()
        return


    def disp_about(self):
        color = colors.WHITE

        self.screen.fill(colors.BLACK)
        self.draw_screen_border(color, tempest.BOX_STYLE_TITLE)
        self.disp_title(f'About {version.prog}', tempest.DEF_FONT_NAME, color)

        dlist = disptext.TextBox(self.screen, self.box_main, font_size=20)

        txt = f'''
{version.prog}, version {version.version}
{version.group_url}


Weather data courtesy of the National Weather Service (NWS)
https://www.weather.gov/ (hourly and daily forecast, weather alerts, and
current conditions)

Radar images courtesy of the NWS (https://radar.weather.gov/)


Water data courtesy of the U.S. Geological Survey (USGS)
https://waterdata.usgs.gov/nwis
'''

        dlist.format(txt)
        dlist.display()
        pygame.display.update()
        return


    def disp_info(self):
        # Display the info screen
        line_color = colors.WHITE
        text_color = colors.WHITE

        self.screen.fill(colors.BLACK)
        self.draw_screen_border(line_color, tempest.BOX_STYLE_TITLE)
        self.disp_time_date(tempest.DEF_FONT_NAME, text_color)

        dtext = disptext.TextBox(self.screen, self.box_main,
                                 font_rsize=self.small_text_height)
        items = {}

        now = util.now()
        logger.debug('current time: %s', now)
        sr, ss = get_sunrise_sunset_strings()
        items.update({'Next sunrise:': sr})
        items.update({'Next sunset:': ss})

        delta = util.sunset(now) - util.sunrise(now)
        items.update({'Daylight today:': util.time_delta_str(delta)})

        logger.debug('calling is_daytime(%s)', now)
        if util.is_daytime(now):
            delta = util.next_sunset() - now
            items.update({'Sunset in:': util.time_delta_str(delta)})
        else:
            delta = util.next_sunrise() - now
            items.update({'Sunrise in:': util.time_delta_str(delta)})

        dtext.name_values(items)
        dtext.display()
        pygame.display.update()
        return


    def is_alert(self):
        # Return True if there is an active weather alert
        if self.get_primary_alert() is None:
            return False
        return True


    def get_primary_alert(self):
        # Return the most severe alert.
        # if more then one of same severity return the first

        worst_alert = None
        alert_cnt = self.nws.get_num_alerts()

        for num in range(alert_cnt):
            alert = self.nws.get_alert(num)
            worst_alert = most_severe(alert, worst_alert)

        return worst_alert


    def disp_alert_detail(self, alertnum):
        # Display the details of a give alert
        self.screen.fill(colors.BLACK)

        line_color = colors.WHITE
        text_color = colors.WHITE

        alert = self.nws.get_alert(alertnum - 1)
        if alert:
            #title = alert.event
            title = alert.headline
            desc = alert.description
        else:
            title = 'This alert ({}) is no longer valid'.format(alertnum)
            desc = ''

        self.draw_screen_border(line_color, tempest.BOX_STYLE_TITLE)
        self.disp_title('Weather Alert Details', tempest.DEF_FONT_NAME,
                        text_color)

        box = self.box_main

        # These are some example texts
        #title = "Freeze Warning"
        #desc =  "* WHAT...Sub-freezing temperatures as low as 30 degrees expected.\n\n* WHERE...Portions of Maryland and Virginia between the Interstate\n81 and 95 corridors as well as the extreme eastern West\nVirginia panhandle.\n\n* WHEN...From midnight tonight to 9 AM EDT Friday.\n\n* IMPACTS...Frost and freeze conditions will kill crops, other\nsensitive vegetation and possibly damage unprotected outdoor\nplumbing." # noqa: E501
        #title = "Red Flag Warning"
        #desc = "...RED FLAG WARNING IN EFFECT FROM 1 PM THIS AFTERNOON TO 7 PM CDT\nTHIS EVENING FOR WINDS AND LOW RELATIVE HUMIDITY OVER VAL VERDE\nCOUNTY...\n\n.A south wind begins the day, but Val Verde county will see an\nearly afternoon wind shift as a dry cold front arrives. Winds will\ninitially increase and switch to the west and northwest in the\nafternoon, and then shift again to the North and northeast in the\nlate afternoon and early evening. Strongest winds are expected\nduring the time of maximum heating, and temperatures in the middle\n90s could lead to minimum humidity values as low as the single\ndigits.\n\nThe National Weather Service in Austin/San Antonio has issued a\nRed Flag Warning for wind and low relative humidity, which is in\neffect from 1 PM this afternoon to 7 PM CDT this evening.\n\n* WINDS...Northwest 10 to 15 mph with gusts up to 25 mph.\n\n* RELATIVE HUMIDITY...As low as 10 percent.\n\n* IMPACTS...In addition to the gusty afternoon wind speeds, the\nshiftiness of the wind direction throughout the day will make\nfire control efforts challenging." # noqa: E501
        #desc = "...The Flood Warning continues for the following rivers in\nLouisiana...\n\nLittle River Of Louisiana Near Rochelle affecting Caldwell, Winn,\nLa Salle and Grant Parishes.\n\nThe Flood Warning continues for\nthe Little River Of Louisiana Near Rochelle.\n* Until further notice.\n* At 9:30 AM CDT Saturday the stage was 37.8 feet.\n* Flood stage is 32.0 feet.\n* Minor flooding is occurring and moderate flooding is forecast.\n* Recent Activity...The maximum river stage in the 24 hours ending\nat 9:30 AM CDT Saturday was 37.8 feet.\n* Forecast...The river is expected to rise to a crest of 38.0 feet\nthis evening.\n* Impact...At 38.0 feet, Ebenezer Tab Road overflows." # noqa: E501
        #desc = "* WHAT...Temperatures 90 to 98.\n\n* WHERE...San Diego County Valleys, Orange County Inland Areas\nand San Bernardino and Riverside County Valleys-The Inland\nEmpire.\n\n* WHEN...Saturday 11 AM PDT to 6 PM PDT.\n\n* IMPACTS...Heat illnesses may occur.\n\n* ADDITIONAL DETAILS...The record high in Riverside today is 96\nset in 2004." # noqa: E501
        #desc = "* WHAT...Seas 4 to 5 ft.\n\n* WHERE...Fire Island Inlet NY to Moriches Inlet NY out 20 nm.\n\n* WHEN...Until 4 PM EDT this afternoon.\n\n* IMPACTS...Conditions will be hazardous to small craft." # noqa: E501

        para_sep = '@PaRaGrApH@'
        newline_sep = '@NeWlInE@'

        # Remove who issued alert to make it shorter
        title = re.sub(r'by NWS.*', '', title)

        # remove any leading periods
        desc = re.sub(r'^\.*', '', desc)
        # remove any periods after nl
        desc = re.sub(r'\n\.*', '\n', desc, re.M)
        # assume '* ' after a space or NL is marking a bullet point
        # and preceed with newline
        desc = re.sub(r'(^|\n| )+\* ', newline_sep + ' * ', desc, re.M)
        # save multiple NLs as a new paragraph
        desc = re.sub(r'\n\n+', para_sep, desc, re.M)
        # save any ...\n as a new paragraph
        desc = desc.replace('...\n', para_sep)
        # remove any other NLs with space
        desc = desc.replace('\n', ' ')
        # replace the paragraph separator with a double NL
        desc = desc.replace(para_sep, '\n\n')
        # replace the newline separator with a NL
        desc = desc.replace(newline_sep, '\n')
        # remove any NLs at the beginning
        desc = re.sub(r'^\n*', '', desc, re.M)

        dtitle = disptext.TextBox(self.screen, box, font_size=20, bold=True)
        dtitle.format(title)
        dtitle.display()
        y = dtitle.get_ymax()

        rect = box.rect()
        rect.move_ip(0, y)
        rect = rect.clip(box.rect())
        ddesc = disptext.TextBox(self.screen, rect, font_size=16, bold=False)
        ddesc.format(desc, para_space=1.5)
        ddesc.display()
        pygame.display.update()
        return


    def disp_alerts(self):
        # Display a summary of all alerts

        self.screen.fill(colors.BLACK)

        line_color = colors.WHITE
        text_color = colors.WHITE

        self.draw_screen_border(line_color, tempest.BOX_STYLE_ALERTS)
        self.disp_title('Weather Alerts', tempest.DEF_FONT_NAME, text_color)

        alert_cnt = self.nws.get_num_alerts()
        if not alert_cnt:
            txt = 'No alerts'
            dtext = disptext.TextBox(self.screen, self.box_alert1,
                                     font_size=30,
                                     color=text_color)
            dtext.format(txt)
            dtext.display()
            pygame.display.update()
            return

        alert_num = 0
        for num in range(alert_cnt):
            alert = self.nws.get_alert(num)

            logger.debug('')
            logger.debug('alert(%d) has fields %s', num, alert)
            logger.debug('Sent: %s', alert.sent)
            logger.debug('Effective: %s, onset: %s',
                         alert.effective, alert.onset)
            logger.debug('Expires: %s, Ends: %s', alert.expires, alert.ends)
            logger.debug('Status: %s', alert.status)
            logger.debug('messageType: %s', alert.messageType)
            logger.debug('Category: %s', alert.category)
            logger.debug('Severity: %s', alert.severity)
            logger.debug('Certainty: %s', alert.certainty)
            logger.debug('Urgency: %s', alert.urgency)
            logger.debug('Event: %s', alert.event)
            logger.debug('Headline: %s', alert.headline)
            logger.debug('AreaDesc: %s', alert.areaDesc)
            #logger.debug('Description: %s', alert.description) ## very long

            alert_num += 1
            text_color = alert_to_color(alert)
            alert_box = self.get_alert_box(alert_num)
            if not alert_box:
                logger.debug('too many alerts, ignored #%d', alert_num)
                continue

            txt = alert.event + '\n'

            dt = nws.alert_starts(alert)
            if dt > util.now():
                txt += 'Starts {}, '.format(dt.strftime("%a %H:%M"))

            dt = nws.alert_expires(alert)
            txt += "Expires {}".format(dt.strftime("%a %H:%M"))

            if alert.messageType == 'Update':
                txt += " (updated)"

            dtext = disptext.TextBox(self.screen, alert_box, font_size=30,
                                     color=text_color)
            dtext.format(txt)
            dtext.display()

        pygame.display.update()
        return


    def screen_cap(self):
        # Save a jpg image of the screen.
        pygame.image.save(self.screen, "screenshot.jpeg")
        logger.info("Screen capture complete")
        return


    def daily_box_to_day(self, box):
        boxes = (self.box_bot1, self.box_bot2, self.box_bot3, self.box_bot4)
        try:
            i = boxes.index(box)
        except ValueError:
            return None

        return self.daily_boxes[i]


    def box_to_hourly_page(self, box):
        # map a daily subwindow box to the corresponsing page name (mode)
        # to display the corresponding hourly graph
        try:
            hourly_map = util.load_hmap('hourly-map.json')
            logger.debug('hourly_map = %s', hourly_map)
        except FileNotFoundError:
            logger.debug('No hourly-map.json file')
            return None

        box_dt = self.daily_box_to_day(box)
        logger.debug('box_dt = %s (type=%s)', box_dt, type(box_dt))
        if not box_dt:
            return None

        for d in hourly_map:
            logger.debug('d = %s (type=%s)', d, type(d))
            dt = hourly_map[d]
            logger.debug('dt = %s (type=%s)', dt, type(dt))
            if util.is_same_day(dt, box_dt):
                return 'h{}'.format(d + 1) # e.g. h1, h2, h3, or h4

        logger.debug('No matching hourly file found')
        return None


    def display_screen(self, mode):
        # Display screen corresponding to mode
        # pylint: disable=too-many-branches

        if mode == 'd1':
            logger.info('displaying daily weather (d1)')
            self.disp_daily(0, 3)
        elif mode == 'd2':
            logger.info('displaying daily weather (d2)')
            self.disp_daily(4, 7)
        elif mode == 'd3':
            logger.info('displaying daily weather (d3)')
            self.disp_daily(8, 11)
        elif mode == 'h1':
            logger.info('displaying hourly weather (h1)')
            self.disp_hourly(1)
        elif mode == 'h2':
            logger.info('displaying hourly weather (h2)')
            self.disp_hourly(2)
        elif mode == 'h3':
            logger.info('displaying hourly weather (h3)')
            self.disp_hourly(3)
        elif mode == 'h4':
            logger.info('displaying hourly weather (h4)')
            self.disp_hourly(4)
        elif mode == 'h5':
            logger.info('displaying hourly weather (h5)')
            self.disp_hourly(5)
        elif mode == 'h6':
            logger.info('displaying hourly weather (h6)')
            self.disp_hourly(6)
        elif mode == 'r':
            logger.info('displaying radar (r)')
            self.disp_radar()
        elif mode == 'i':
            logger.info('displaying info (i)')
            self.disp_info()
        elif mode == 'a':
            logger.info('displaying alerts (a)')
            self.disp_alerts()
        elif mode == 'a1':
            logger.info('displaying alert (a1)')
            self.disp_alert_detail(1)
        elif mode == 'a2':
            logger.info('displaying alert (a2)')
            self.disp_alert_detail(2)
        elif mode == 'a3':
            logger.info('displaying alert (a3)')
            self.disp_alert_detail(3)
        elif mode == 'a4':
            logger.info('displaying alert (a4)')
            self.disp_alert_detail(4)
        elif mode == 'a5':
            logger.info('displaying alert (a5)')
            self.disp_alert_detail(5)
        elif mode == 'times':
            logger.info('displaying update times (times)')
            self.disp_update_times()
        elif mode == 'about':
            logger.info('displaying about (about)')
            self.disp_about()
        else:
            logger.info('Unhandled mode (%s)', mode)
            raise ValueError

        return True


# end class MyDisplay


def next_page(cur, reverse=False):
    # Get next (or prev if reverse) page (mode) in the cycle of pages
    # If the current page is not in the below set, we'll always return the
    # first page in the list
    pages = ['d1', 'd2', 'd3',
             'h1', 'h2', 'h3', 'h4', 'h5', 'h6',
             'r', 'a', 'i']
    return list_next_circular(cur, pages, reverse=reverse)


def receive_signal(sig, unused_frame):
    # Signal handle for SIGCHLD and termination signals
    global running
    global my_fetcher

    logger.debug('pid %d received signal: %s (%d)',
                 os.getpid(), signal.Signals(sig).name, sig)

    if sig == signal.SIGCHLD:
        pid, status = os.waitpid(-1, os.WNOHANG | os.WUNTRACED | os.WCONTINUED)
        if os.WIFCONTINUED(status) or os.WIFSTOPPED(status):
            logger.debug('PID %d continued or stopped -- ignored', pid)
            return

        if os.WIFSIGNALED(status):
            child_sig = os.WTERMSIG(status)
            logger.error('PID %d terminated by signal %s', pid,
                         signal.Signals(child_sig).name)

        if os.WIFEXITED(status):
            code = os.WEXITSTATUS(status)
            if code:
                logger.error('PID %d exited with code %s', pid, code)
            else:
                logger.info('PID %d exited normally', pid)

        if not running: #pylint: disable=used-before-assignment
            # probbly should not get this as we should have set
            # CHLD to SIG_IGN by now
            logger.debug('Got SIGCHLD while stopping -- ignored')
            return

        return

    logger.info('Exiting due to %s', signal.Signals(sig).name)
    running = False

    if sig != signal.SIGCHLD:
        signal.signal(signal.SIGCHLD, signal.SIG_DFL)
        my_fetcher.stop(force=1)

    logger.info('Exiting via signal')
    screen.Screen.close()
    logger.stop()
    pfile.close()
    os._exit(2) # pylint: disable=protected-access
    return


# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

def main():
    global running
    global my_fetcher
    global my_disp

    # Initial mode is daily weather
    mode = 'd1'
    last_mode = ''
    last_minute = -1

    # Create an instance of the display class.
    # This must be done before the the datafetch process is created
    # so we can pass in the correct screen_size to the child so it
    # can correctly scale images
    my_disp = MyDisplay()

    # For changing screen brightness (on supported systems)
    my_bright = brightness.Brightness()

    # block some signals, start child proc (data fetcher), then
    # register signal handlers and unblock signals.  The unblock will
    # be done here (parent) and in the child.  This should prevent
    # race conditions.
    sigs = [signal.SIGINT, signal.SIGTERM]
    signal.pthread_sigmask(signal.SIG_BLOCK, sigs)

    # Start data fetching child
    my_fetcher = datafetch.DataFetch()
    my_fetcher.start(sigs)

    # Capture sigs and then unblock them
    signal.signal(signal.SIGCHLD, receive_signal)
    for sig in sigs:
        signal.signal(sig, receive_signal)

    signal.pthread_sigmask(signal.SIG_UNBLOCK, sigs)

    # We don't use this and it is generates a lot of events.
    # Blocking it cuts down on CPU load and reduced debugging output as well.
    pygame.event.set_blocked(pygame.ACTIVEEVENT)

    # Initialize time of last user input to now (start time)
    last_input_time = time.time()

    # Register a timer event so we can do periodic tasks
    timer_event = pygame.event.Event(pygame.USEREVENT)
    pygame.event.post(timer_event)
    pygame.time.set_timer(pygame.USEREVENT, 1000) # fire 1/sec

    # Clear any pending mouse motion and
    # init the mouse motion tracker (my_mouse)
    pygame.mouse.get_rel()
    my_mouse = mouse.Mouse()

    need_update = True

    # Force a load to check for old version of file
    try:
        util.load_hmap('hourly-map.json')
    except FileNotFoundError:
        pass

    while running:
        # load data if needed
        if my_disp.load_data() is True:
            need_update = True

        # update display if mode change or if minute changed (for time)
        minute = time.localtime().tm_min
        if mode != last_mode or minute != last_minute:
            need_update = True

        if need_update:
            logger.debug('need_update: mode %s->%s; minute %s->%s',
                         last_mode, mode, last_minute, minute)
            last_mode = mode
            last_minute = minute
            need_update = False
            # XXX run twice to workaround a display issue on the top
            # of the screen when some data from previous display lingers
            # till the next update.  Seen on Raspberry Pi 12 (bookworm)
            # with Xorg.
            ok = my_disp.display_screen(mode)
            ok = my_disp.display_screen(mode)
            if not ok:
                need_update = True

        # Wait for a pygame event
        event = pygame.event.wait()
        #logger.debug('Got event %s', event)

        # Update the current time after waiting
        curr_time = time.time()

        if not my_fetcher.is_alive():
            logger.info('Child no longer alive, exiting')
            running = False

        if event.type == pygame.QUIT:
            running = False

        if event.type == pygame.VIDEORESIZE:
            logger.debug('Resizing window to %s', event.size)
            screen.Screen.set_size(event.size)
            my_disp.new_window_size()
            need_update = True

        if event.type == pygame.WINDOWFOCUSGAINED:
            logger.debug('Got window focus')
            need_update = True

        if event.type in (pygame.KEYDOWN, pygame.KEYUP, pygame.MOUSEBUTTONDOWN,
                          pygame.MOUSEBUTTONUP, pygame.MOUSEMOTION):
            last_input_time = curr_time
            #logger.debug('user interaction at %s', last_input_time)

        # Update idle time now that the two inputs have been updated
        idle_time = curr_time - last_input_time

        if event.type == pygame.KEYDOWN:
            logger.debug('Got KEYDOWN: %s', event.key)
            mode = new_mode_from_keypress(mode, event.key)

        if event.type == pygame.MOUSEBUTTONDOWN:
            my_mouse.button_down()

        if event.type == pygame.MOUSEMOTION:
            rel = pygame.mouse.get_rel()
            my_mouse.motion(rel)

        if event.type == pygame.MOUSEBUTTONUP:
            mouse_action = my_mouse.button_up()
            if mouse_action == mouse.SWIPE_LEFT:
                mode = next_page(mode)
            elif mouse_action == mouse.SWIPE_RIGHT:
                mode = next_page(mode, reverse=True)
            elif mouse_action == mouse.SWIPE_UP:
                my_bright.inc(10)
            elif mouse_action == mouse.SWIPE_DOWN:
                my_bright.dec(10)
            elif mouse_action == mouse.CLICK:
                logger.debug('got click')
                pos = pygame.mouse.get_pos()
                mode = my_disp.handle_click(pos, mode)
                need_update = True # incase we need to remove a menu
                if mode == 'quit':
                    running = False

        # Adjust brightness based on time of day and idle_time
        alert = my_disp.is_alert()
        my_bright.auto_adj(idle_time, alert)

    # end 'while running' loop

    logger.info('Quitting')
    screen.Screen.exiting()
    signal.signal(signal.SIGCHLD, signal.SIG_DFL)
    my_fetcher.stop()
    screen.Screen.close()
    return
    # end of main()



def start():
    try:
        # Create the directory to store our downloaded data
        os.makedirs(config.CACHE_DIR, exist_ok=True)
        os.makedirs(config.STATE_DIR, exist_ok=True)

        # Create our pidfile
        pfile_name = config.CACHE_DIR / 'pid'
        global pfile
        pfile = pidfile.PidFile(pfile_name)
    except pidfile.AlreadyRunning:
        msg = 'Already running according to {}'.format(pfile_name)
        print(msg)
        screen.Screen.simple_message(msg)
        time.sleep(2)
        screen.Screen.close()
        sys.exit(1)


    #
    # Add some derived values to config modules namespace
    # Convention is for uppercase for values in config.py and
    # lower case for other items added like here.
    config.hourly_dir = config.CACHE_DIR / 'hourly'

    #
    # Assorted globals
    #
    global running
    global my_fetcher
    global my_disp
    running = True
    my_fetcher = None # set for real in main()
    my_disp = None # set for real in main()

    # Now run the main function
    # Catch any unhandled exception and exit
    try:
        main()
    except Exception:
        logger.critical('Got unhandled exception', exc_info=True)
        pfile.close()
        del my_disp
        #del my_fetcher
        my_fetcher.stop(force=1)
        # sys.exit() will hang, probably due to child still running
        os._exit(3) # pylint: disable=protected-access

    logger.info('--end--')
    logger.stop()
    pfile.close()


if __name__ == "__main__":
    start()
