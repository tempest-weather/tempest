#
# Define colors
#
# Copyright (c) 2020 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
# SPDX-License-Identifier: GPL-3.0-or-later
#

# Some color values came from https://clrs.cc/
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
GRAY = (0xaa, 0xaa, 0xaa)

RED = (0xff, 0x41, 0x36)
ORANGE = (0xff, 0x85, 0x1b)
YELLOW = (0xff, 0xdc, 0)
GREEN = (0x2e, 0xcc, 0x40)
AQUA = (0x7f, 0xdb, 0xff)
BLUE = (0, 0x74, 0xd9)
