#
# Handle different units
#
# Copyright (c) 2020,2023 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
# SPDX-License-Identifier: GPL-3.0-or-later
#

# local imports
import tempest.lib
from tempest.lib import config
from tempest.lib import util
from tempest.lib import logger


us_units = {
    # kind  Abrv  digits(after .)
    'Temp': ('F', 0),
    'WaterDepth': (' ft', 2),
    'SnowDepth': (' in', 0),
    'Elevation': (' ft', 0),
    'Speed': (' mph', 0),
    'Pressure': (' inHg', 2),
    '%': ('%', 0),
    'Distance': (' mi', 0),
}

si_units = {
    'Temp': ('C', 1),
    'WaterDepth': (' m', 2),
    'SnowDepth': (' cm', 0),
    'Elevation': (' m', 0),
    'Speed': (' kph', 0),
    'Pressure': (' mbar', 0),
    '%': ('%', 0),
    'Distance': (' km', 0),
}


if config.UNITS == 'us':
    units = us_units
else:
    units = si_units


def abrv(kind):
    if kind == 'Temp':
        return tempest.lib.UNICODE_DEGREE + units[kind][0]
    return units[kind][0]


def digits(kind):
    return units[kind][1]


def pretty_wind(speed, bearing):
    if speed == 0:
        return 'Calm'

    if bearing is None:
        bearing = 'Vrbl'
    else:
        if bearing > 360 or bearing < 0:
            logger.error('Unexpected value for bearing, %s', bearing)
        else:
            bearing = util.deg_to_compass(bearing)

    if speed is None:
        speed = '?'
    else:
        speed = '{0:.{1}f}'.format(speed, digits('Speed'))

    return '{} @ {}{}'.format(bearing, speed, abrv('Speed'))


def pretty_water(temp, level):
    ptemp = pretty(temp, 'Temp')
    plevel = pretty(level, 'WaterDepth')
    return '{}, {}'.format(ptemp, plevel)


def pretty(val, kind, units=True):
    if val is None:
        if kind is None:
            return '?'
        if units:
            return '?{}'.format(abrv(kind))
        return '?'

    if kind is None:
        return '{:.0f}'.format(val)

    if units:
        return '{0:.{1}f}{2}'.format(val, digits(kind), abrv(kind))
    return '{0:.{1}f}'.format(val, digits(kind))


def convert(val, uom):
    if val is None:
        return val

    if isinstance(val, str):
        val = float(val)

    # Some units are prefixed by 'unit' or 'wmoUnit'.  They seem to have the
    # same possible value, so replace one for the other reduce tests below
    uom = uom.replace('wmoUnit:', 'unit:')

    if uom in ('unit:degree_(angle)', 'unit:percent'):
        # nothing needed for these
        pass
    elif config.UNITS == 'us':
        if uom in ('unit:degF', 'F', 'ft', 'miles'):
            pass
        elif uom in ('unit:degC', 'deg C', 'C'):
            val = val * 9 / 5 + 32 # C to F
        elif uom == 'unit:mm':
            val = val / 25.4 # mm to inches
        elif uom == 'unit:m':
            val = val * 3.281 # m to feet
        elif uom == 'unit:m_s-1':
            val = val * 2.237 # m/s to mph
        elif uom == 'unit:km_h-1':
            val = val / 1.609 # kph to mph
        elif uom in ('kilometers', 'km'):
            val = val / 1.609 # km to mi
        elif uom == 'knots':
            val = val * 1.151 # knots to mph
        elif uom == 'unit:Pa':
            val = val / 3386 # Pa to inHg
        else:
            logger.error('Unable to convert "%s" to "us" units', uom)
    elif config.UNITS == 'si':
        if uom in ('unit:degC', 'deg C', 'C', 'unit:mm', 'unit:mm',
                   'kilometers', 'km'):
            pass
        elif uom in ('unit:degF', 'F'):
            val = (val - 32) * 5 / 9 # F to C
        elif uom == 'unit:in':
            val = val * 25.4 # inches to mm
        elif uom in ('unit:ft', 'ft'):
            val = val / 3.281 # feet to m
        elif uom == 'unit:m_s-1':
            val = val * 3.6 # m/s to kph
        elif uom == 'mph':
            val = val * 1.609 # mph to kph
        elif uom == 'miles':
            val = val * 1.609 # miles to km
        elif uom == 'knots':
            val = val * 1.852 # knots to kph
        elif uom == 'unit:Pa':
            val = val / 100 # Pa to mbar
        else:
            logger.error('Unable to convert "%s" to "si" units', uom)

    return val


def convert_df(df, column, uom):
    df[column] = convert(df[column], uom)
    return


if __name__ == "__main__":
    # test code
    print('Pretty printing values...')
    print(' Temp 55.321:      ', pretty(54.321, 'Temp'))
    print(' Wind 270/21:      ', pretty_wind(21, 270))
    print(' WaterDepth 1.234: ', pretty(1.234, 'WaterDepth'))
    print(' SnowDepth 8.2:    ', pretty(8.2, 'SnowDepth'))
    print(' Temp None:        ', pretty(None, 'Temp'))
    print(' No kind 5.67:     ', pretty(5.67, None))
