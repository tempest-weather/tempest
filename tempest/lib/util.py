#
# Copyright (c) 2020-2023 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
# SPDX-License-Identifier: GPL-3.0-or-later
#

import os
import datetime
import contextlib

import tzlocal
from suntime import Sun
import jsonpickle
import pickle

# local imports
from tempest.lib import config
from tempest.lib import units
from tempest.lib import logger


def next_day(day):
    # Return a datetime object corresponding to the day after 'day'
    return day + datetime.timedelta(1)


def prev_day(day):
    # Return a datetime object corresponding to the day before 'day'
    return day - datetime.timedelta(1)


def localstrftime(dt, fmt='%Y-%m-%d %H:%M'):
    # Convert a datetime to a string in the local timezone
    return dt.astimezone(tz=tz()).strftime(fmt)


def get_start_of_day(day):
    # Return datetime objects for the start of the given day
    return day.replace(hour=0, minute=0, second=0, microsecond=0)


def get_end_of_day(day):
    # Return datetime objects for the end of the given day
    start = get_start_of_day(day)
    return next_day(start)


def get_day_string(dt):
    # Return a string for the given day ('Today' or e.g. 'Sunday')
    if is_same_day(now(), dt):
        return 'Today'
    return dt.strftime("%A")


def tz():
    # Return local timezone
    return tzlocal.get_localzone()


def now():
    # Return current time as a datetime object
    return datetime.datetime.now(tz=tz())


def get_time_of_day():
    # Return the current time of day as a time object (hr, min, sec, msec)
    return now().time()


def next_sunrise(dt=None):
    # Return a datetime for the next sunrise after dt
    # Use current time is dt is None
    if dt is None:
        dt = now()

    sr = sunrise(dt)
    if sr < dt:
        sr = sunrise(dt + datetime.timedelta(days=1))

    return sr


def next_sunset(dt=None):
    # Return a datetime for the next sunset after dt
    # Use current time is dt is None
    if dt is None:
        dt = now()

    ss = sunset(dt)
    if ss < dt:
        ss = sunset(dt + datetime.timedelta(days=1))

    return ss

def sunrise(dt=None):
    # Return datetime for sunrise on given date
    # Date (dt) can be a datetime, and offset in days from today,
    # or if not specified, today
    if isinstance(dt, int):
        dt = now() + datetime.timedelta(days=dt)

    if dt is None:
        dt = now()

    sun = Sun(config.LAT, config.LON)
    return sun.get_sunrise_time(dt, time_zone=tz())


def sunset(dt=None):
    # Return datetime for sunset on given date
    # Date (dt) can be a datetime, and offset in days from today,
    # or if not specified, today
    if isinstance(dt, int):
        dt = now() + datetime.timedelta(days=dt)

    if dt is None:
        dt = now()

    sun = Sun(config.LAT, config.LON)

    # Workaround for bug https://github.com/SatAgro/suntime/issues/12
    # that affects suntime-1.2.5
    # If sunset is before sunrise, set it to the next day
    ss = sun.get_sunset_time(dt, time_zone=tz())
    sr = sun.get_sunrise_time(dt, time_zone=tz())
    if ss < sr:
        ss = next_day(ss)
    return ss


def is_daytime(dt=None):
    # Return True if give time (default now) is currently daytime
    logger.debug('sunrise:     %s', sunrise())
    logger.debug('sunset:      %s', sunset())
    logger.debug('sunrise(dt): %s', sunrise(dt))
    logger.debug('sunset(dt) : %s', sunset(dt))
    logger.debug('dt:          %s', dt)

    if dt is None:
        dt = now()

    if sunrise(dt) < dt < sunset(dt):
        logger.debug('daytiime')
        return True

    logger.debug('nighttiime')
    return False


def is_same_day(this_day, target_day):
    # Return True if this_day is in the same day as target_day
    # Include 2400hours (i.e. 0000 next day) as the same day

    this_start = get_start_of_day(this_day)
    target_start = get_start_of_day(target_day)

    if this_start == target_start:
        return True

    if this_start == this_day:
        # must be 0000 hours this_day,
        # try matching previous day to target to handle midnight
        if prev_day(this_start) == target_start:
            return True

    return False


def time_delta_str(delta):
    total_sec = delta.total_seconds()
    hours, rem = divmod(total_sec, 60 * 60)
    #minutes, seconds = divmod(rem, 60)
    minutes = rem // 60

    if hours == 0 and minutes == 0:
        return 'less than a minute'

    s = ''
    if hours:
        s += '{}h '.format(int(hours))
    s += '{}m'.format(int(minutes))
    return s


def time_str_and_delta(dt):
    dt_str = localstrftime(dt)
    delta = now() - dt
    return '{} ({} ago)'.format(dt_str, time_delta_str(delta))


def get_gust_values():
    # Wind Gust: "Is a sudden, brief increase in speed of the wind.
    #  According to U.S. weather observing practice, gusts are reported
    #  when the peak wind speed reaches at least 16 knots and the variation
    #  in wind speed between the peaks and lulls is at least 9 knots. The
    #  duration of a gust is usually less than 20 seconds.
    #  https://graphical.weather.gov/definitions/defineWindGust.html
    gust_min = 16 # knots
    gust_delta = 9 # knots
    return (units.convert(gust_min, 'knots'),
            units.convert(gust_delta, 'knots'))


def floor(v, mult):
    # Return a value <= to v that is the nearest multiple of mult
    return mult * (v // mult)


def ceil(v, mult):
    # Return a value >= to v that is the nearest multiple of mult
    return mult * ((v // mult) + 1)


def rm_f(f):
    # Like running rm -f <f>
    with contextlib.suppress(FileNotFoundError):
        os.remove(f)
    return


def df_indent(df):
    # Indent the string representation of a pandas.DataFrame
    # Useful for printing with logging module
    indent = '  '
    return indent + df.to_string().replace('\n', '\n' + indent)


def list_indent(lst):
    if lst is None:
        return "None"

    s = '\n'
    for ll in lst:
        s += '  {}\n'.format(ll)
    return s.rstrip()


def dict_indent(dct):
    if dct is None:
        return "None"

    s = '\n'
    for d in dct:
        s += '  {}: {}\n'.format(d, dct[d])
    return s.rstrip()


def save_data(file_name, data):
    path_name = '{}/{}'.format(config.CACHE_DIR, file_name)
    tmp_file = '{}.part'.format(path_name)

    json = jsonpickle.encode(data, keys=True)

    with open(tmp_file, 'w') as fp:
        fp.write(json)

    os.rename(tmp_file, path_name)


def load_data(file_name):
    path_name = '{}/{}'.format(config.CACHE_DIR, file_name)

    with open(path_name) as fp:
        json = fp.read()

    data = jsonpickle.decode(json, keys=True)
    return data


def save_hmap(file_name, data):
    path_name = '{}/{}'.format(config.CACHE_DIR, file_name)
    tmp_file = '{}.part'.format(path_name)

    with open(tmp_file, 'wb') as fp:
        pickle.dump(data, fp)

    os.rename(tmp_file, path_name)


def load_hmap(file_name):
    path_name = '{}/{}'.format(config.CACHE_DIR, file_name)

    try:
        with open(path_name, 'rb') as fp:
            data = pickle.load(fp)

    except pickle.UnpicklingError:
        rm_f(path_name)
        logger.info('Bad hourly-map.json file -- removed')
        data = None

    return data


def deg_to_compass(degrees):
    # Convert degrees to one of 16 wind compass rose names
    # https://en.wikipedia.org/wiki/Points_of_the_compass#16-wind_compass_rose
    val = round(degrees / 22.5)
    dirs = ["N", "NNE", "NE", "ENE",
            "E", "ESE", "SE", "SSE",
            "S", "SSW", "SW", "WSW",
            "W", "WNW", "NW", "NNW"]
    return dirs[val % 16]


def date_from_file(fname):
    # Return the date (datetime) of the given file's mtime
    # If file does not exist, returns None
    if not os.path.exists(fname):
        return None

    mtime = os.path.getmtime(fname)
    return datetime.datetime.fromtimestamp(mtime, tz=tz())
