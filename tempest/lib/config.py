#
# Copyright (c) 2023 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
# SPDX-License-Identifier: GPL-3.0-or-later
#

import configparser
import json
from pathlib import Path

config = configparser.ConfigParser(inline_comment_prefixes=('#'))

xdgconfig = f'{Path.home()}/.config/tempest/tempest.conf'
flist = config.read((xdgconfig, 'tempest.conf'))
print(f'Read config from {flist}')


if not config.has_section('tempest'):
    #print('No [tempest] section found in config file, adding dummy')
    config.add_section('tempest')

section = config['tempest']


LOG_LEVEL = section.get('log_level', 'info')

xdgcache = Path.home() / '.cache/tempest'
xdgstate = Path.home() / '.local/state/tempest'
CACHE_DIR = Path(section.get('cache_dir', xdgcache))
STATE_DIR = Path(section.get('state_dir', xdgstate))
LOG_DIR = Path(section.get('log_dir', xdgstate / 'log'))

ICON_SET = section.get('icon_set', 'default')
UNITS = section.get('units', 'us')
FULLSCREEN = section.get('fullscreen', 'auto')

WEATHER_CHECK_INTERVAL = section.getint('weather_check_interval', 900)
WATER_CHECK_INTERVAL = section.getint('water_check_interval', 3600)
RADAR_CHECK_INTERVAL = section.getint('radar_check_interval', 900)

BLANK_TIMES = json.loads(section.get('blank_times', '[]'))
BLANK_IDLE_TIME = section.getint('blank_idle_time', 1800)

RADAR_RAIN_SNOW = section.getint('radar_rain_snow', 45)



# XXX need better error handling or default
LAT = section.getfloat('LAT')
LON = section.getfloat('LON')
STATIONS = json.loads(section.get('stations', '[]'))
RADAR_SITES = json.loads(section.get('radar_sites', '[]'))
WATER_SITE = section.get('water_site', None)
