#
# Copyright (c) 2020,2023 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
# SPDX-License-Identifier: GPL-3.0-or-later
#


import os
import logging
from logging.handlers import RotatingFileHandler

# Local imports
from tempest.lib import config


# These will be reassigned in init() and they will be functions
# used for logging messages at the corresponding level.  From other
# modules, they will be called like: logger.debug(msg)
debug = None
info = None
warning = None
error = None
critical = None


# Maps between log level string (such as in config.py) to actual level
log_level_map = {
    'debug': logging.DEBUG,
    'info': logging.INFO,
    'warning': logging.WARNING,
    'error': logging.ERROR,
    'critical': logging.CRITICAL,
}


def init(log_name, level='config'):
    # Initialize the logging infrastructure
    log = logging.getLogger(log_name)

    if str(level) == 'config':
        level = config.LOG_LEVEL

    level = log_level_map[level]

    fmt = '{asctime} {levelname:5} {name}[{process}] {module}/' + \
        '{funcName}() {lineno} : {message}'
    log_formatter = logging.Formatter(fmt, style='{')

    # stderr handler
    log_handler = logging.StreamHandler()
    log_handler.setFormatter(log_formatter)
    log.addHandler(log_handler)

    if log_name:
        # file handler
        os.makedirs(config.LOG_DIR, exist_ok=True)
        log_file = '{}/{}.log'.format(config.LOG_DIR, log_name)
        log_handler = RotatingFileHandler(log_file, maxBytes=10000000,
                                          backupCount=4)

        # force rollover at startup
        log_handler.doRollover()

        log_handler.setFormatter(log_formatter)
        log.addHandler(log_handler)
        log.info('Logging setup, log_name=%s', log_name)

    log.setLevel(level)

    global debug
    global info
    global warning
    global error
    global critical
    debug = log.debug
    info = log.info
    warning = log.warning
    error = log.error
    critical = log.critical
    return


def stop():
    logging.shutdown()
    return
