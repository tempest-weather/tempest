#
# Copyright (c) 2020,2023 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
# SPDX-License-Identifier: GPL-3.0-or-later
#

import io

import gi
gi.require_version('Rsvg', '2.0')
from gi.repository import Rsvg
from gi.repository import GLib, Gio
import cairo
import pygame

# local imports
from tempest.lib import logger


def svg2png(sfile, pfile, size):
    # Convert an svg to png file with the given size (x&y are the same)
    new_width = size
    new_height = size

    try:
        svg = Rsvg.Handle.new_from_file(str(sfile))
    except GLib.Error as e:
        if e.matches(Gio.io_error_quark(), Gio.IOErrorEnum.NOT_FOUND):
            logger.error('Unable to open %s, File not found', sfile)
            raise FileNotFoundError
        if e.matches(Gio.io_error_quark(), Gio.IOErrorEnum.IS_DIRECTORY):
            logger.error('Unable to open %s, it is a directory', sfile)
            raise IsADirectoryError
        else:
            raise

    orig_width = svg.props.width
    orig_height = svg.props.height

    surface = cairo.SVGSurface(None, new_width, new_height)
    context = cairo.Context(surface)
    context.save()
    context.scale(new_width / orig_width,
                  new_height / orig_height)
    svg.render_cairo(context)
    context.restore()

    surface.write_to_png(pfile)
    surface.finish()
    return


def load_file(fname, size):
    # Load an SVG icon file and convert to a pygame surface
    bio = io.BytesIO()
    svg2png(fname, bio, size)
    bio.seek(0)
    surface = pygame.image.load(bio).convert_alpha()
    bio.close()
    return surface
