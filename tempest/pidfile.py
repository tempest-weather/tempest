#
# Copyright (c) 2020 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
# SPDX-License-Identifier: GPL-3.0-or-later OR MIT
#
# Based in part on http://code.activestate.com/recipes/577911/
# which is dual licensed under MIT and GPL
#

import os
import fcntl

# Note: do not use logger, it is not active when this is called


class AlreadyRunning(Exception):
    pass


class PidFile:
    # Manage a PID file

    def __init__(self, fname):
        # Create a PID file
        self.fname = fname
        self.fp = open(fname, 'a+')
        try:
            fcntl.flock(self.fp.fileno(), fcntl.LOCK_EX | fcntl.LOCK_NB)
        except IOError:
            raise AlreadyRunning

        self.fp.seek(0)
        self.fp.truncate()
        self.fp.write(str(os.getpid()))
        self.fp.flush()
        return


    def close(self):
        self.fp.close()
        os.remove(self.fname)
        return


    def __repr__(self):
        s = ''
        if self.__class__.__module__ != "__main__":
            s = '{}.'.format(self.__class__.__module__)

        s += "{}(fname='{}')".format(self.__class__.__qualname__, self.fname)
        return s


    def __str__(self):
        return 'pidfile={}'.format(self.fname)
