#
# Detect mouse swipes and clicks
#
# Copyright (c) 2020,2023 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
# SPDX-License-Identifier: GPL-3.0-or-later
#

import time

# local imports
from tempest.lib import logger


# Public constants
CLICK = 0
SWIPE_RIGHT = 1
SWIPE_LEFT = 2
SWIPE_DOWN = 3
SWIPE_UP = 4

# Other constants
MIN_SWIPE = 100
MAX_CLICK = 15

# amount of time consecutive mouse motion events are considered the same motion
MOTION_EVENT_TIME_DELTA = 0.2


class Mouse:
    def __init__(self):
        self.down = False
        self._reset()
        return

    def _reset(self):
        self.rel = [0, 0]
        self.motion_time = time.time()
        logger.debug('mouse reset')
        return


    def motion(self, rel):
        now = time.time()

        if self.down is False:
            # Don't record motion if button is not pressed
            return

        if now - self.motion_time < MOTION_EVENT_TIME_DELTA:
            self.rel[0] += rel[0]
            self.rel[1] += rel[1]
            self.motion_time = now
        else:
            self._reset()

        logger.debug('delta=%s, result=%s', rel, self)
        return


    def button_down(self):
        self.down = True
        self._reset()
        return


    def button_up(self):
        logger.debug('rel=%s', self.rel)

        self.down = False
        x, y = self.rel
        x_mag = abs(x)
        y_mag = abs(y)

        if x_mag >= MIN_SWIPE and y_mag >= MIN_SWIPE:
            logger.debug('Diagonal swipe -- ignoring')
            return None

        if x_mag >= MIN_SWIPE:
            if x > 0:
                return SWIPE_RIGHT
            return SWIPE_LEFT

        if y_mag >= MIN_SWIPE:
            if y > 0:
                return SWIPE_DOWN
            return SWIPE_UP

        if x_mag < MAX_CLICK and y_mag < MAX_CLICK:
            return CLICK

        return None


    def __repr__(self):
        obj = ''
        if self.__class__.__module__ != "__main__":
            obj = '{}.'.format(self.__class__.__module__)

        obj += self.__class__.__qualname__

        return '{}(rel={}, time={})'.format(obj, self.rel, self.motion_time)
