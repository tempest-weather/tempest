#
# Fetch water data
#
# Copyright (c) 2020,2023 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
# SPDX-License-Identifier: GPL-3.0-or-later
#

# Info on water data collection
# https://waterdata.usgs.gov/nwis?automated_retrieval_info
# https://waterwatch.usgs.gov/wqwatch/


import os
import time
import datetime

# local imports
from tempest.lib import util
from tempest.lib import logger
from tempest.lib import config
from tempest.lib import units
from tempest import urlcache
from tempest import jsonobj


class Water:
    def __init__(self):
        self.urlcache = urlcache.Url(config.CACHE_DIR / 'web')

        self.base_url = 'http://waterservices.usgs.gov/nwis/iv'
        self.water_file = '{}/water.json'.format(config.CACHE_DIR)
        self.water = jsonobj.Json(self.water_file)

        # These are the parameterCd in the request and
        # variable code values in the json response
        self.var_height = '00065'
        self.var_temp = '00010'

        self.height = None
        self.temp = None
        return


    def fetch(self):
        # Downloads water data
        if not config.WATER_SITE:
            # No water data wanted
            return False

        param_cd = '{},{}'.format(self.var_height, self.var_temp)
        params = {
            'format': 'json',
            'site': config.WATER_SITE,
            'parameterCd': param_cd
        }
        result = self.urlcache.get(self.base_url, params)
        logger.debug('result is %s', result)
        if result:
            logger.debug('saving data')
            self.water.set(result.json())
            self.water.save()
            self.extract()

        return True


    def need(self):
        if not config.WATER_SITE:
            return False

        logger.debug('water_file is %s', self.water_file)
        if not os.path.exists(self.water_file):
            logger.info('No water file, need to fetch')
            return True

        age = time.time() - os.path.getmtime(self.water_file)
        if age > config.WATER_CHECK_INTERVAL:
            logger.info('Water data old (%d sec), need to fetch', age)
            return True

        return False


    def load(self):
        logger.info('Loading water data')
        self.water.load()
        self.extract()
        return


    def need_reload(self):
        return self.water.need_reload()


    def extract(self):
        # Did through the json and pull out the height and temp
        self.gage_height = None
        self.temperature = None

        if self.water.obj is None:
            logger.debug('No water data')
            return

        now = datetime.datetime.now(tz=util.tz())
        max_delta = datetime.timedelta(hours=12)

        for ts in self.water.obj.value.timeSeries:
            var = ts.variable.variableCode[0].value
            var_name = ts.variable.variableName
            logger.debug('Water site %s', ts.sourceInfo.siteName)
            logger.debug('Looking at %s (%s)', var, var_name)
            unit = ts.variable.unit.unitCode
            no_data_value = ts.variable.noDataValue
            for v in ts.values:
                value = float(v.value[0].value)
                method = v.method[0].methodID
                dt = datetime.datetime.fromisoformat(v.value[0].dateTime)
                logger.debug(' method %s --> %s %s %s',
                             method, value, unit, dt)
                if value == no_data_value:
                    logger.debug('  no data value, skipping')
                    continue

                if now - dt > max_delta:
                    logger.debug('  old, skipping')
                    continue

                logger.debug('  good')
                value = units.convert(value, unit)

                if var == self.var_height:
                    self.height = value
                elif var == self.var_temp:
                    self.temp = value
                else:
                    logger.error(' unknown variable %s (%s), skipped',
                                 var, var_name)

        return


    def get_date(self):
        return util.date_from_file(self.water_file)


    def invalidate(self):
        self.water.remove()


if __name__ == "__main__":
    logger.init(None)
    w = Water()
    w.fetch()
    w.load()
