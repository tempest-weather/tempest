#
# Formatting and display text in pygame
#
# Copyright (c) 2020,2023 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
# SPDX-License-Identifier: GPL-3.0-or-later
#

import sys

import pygame

# Local imports
from tempest.lib import logger
from tempest.lib import colors
from tempest import box
from tempest import screen


MIN_FONT_SIZE = 6


def adj_font_size(surface, font_name, size, bold=True, italic=False):
    # Adjust font size for different screen/window sizes.
    # The give font size is for a standard screen.DEF_SIZE window.
    # This will scale that to whatever the current window size happens to be.
    scrn_size = surface.get_width(), surface.get_height()
    if scrn_size == screen.DEF_SIZE:
        return size

    font = pygame.font.SysFont(font_name, size, bold=bold, italic=False)
    w, h = font.size('X')
    w_max = round(w / screen.DEF_SIZE[0] * scrn_size[0])
    h_max = round(h / screen.DEF_SIZE[1] * scrn_size[1])
    logger.debug('Max size is %s,%s', w_max, h_max)
    logger.debug('Size of "X" is %s,%s', w, h)
    logger.debug('Initial font size is %s', size)

    if w < w_max or h < h_max:
        # We can go bigger (maybe)
        s = size + 1
        while True:
            font = pygame.font.SysFont(font_name, s, bold=bold, italic=False)
            w, h = font.size('X')
            logger.debug('Trying font size %s (w,h)=%s', s, (w, h))
            if w > w_max or h > h_max:
                logger.debug('Final font size is %s', s - 1)
                return s - 1
            s += 1

    else:
        # must go smaller (or same)
        for s in range(size, MIN_FONT_SIZE - 1, -1):
            logger.debug('Trying font size %s', s)
            font = pygame.font.SysFont(font_name, s, bold=bold, italic=False)
            w, h = font.size('X')
            logger.debug('Trying font size %s (w,h)=%s', s, (w, h))
            if w <= w_max and h <= h_max:
                logger.debug('Final font size is %s', s)
                return s
    return s


def _max_size(font, strings):
    # Return the max size of all the strings in the list 'strings'
    # Size is based on the give font
    max_x = 0
    max_y = 0

    for s in strings:
        x, y = font.size(s)
        logger.debug('size of "%s" is %d, %d', s, x, y)
        if x > max_x:
            max_x = x
        if y > max_y:
            max_y = y

    return max_x, max_y


def _break_text(text, font, width):
    # Split a block of text into a line and the remainder
    # Returns a tuple of, the line, length (pixels), and the rest of the text
    para = False
    i = 1
    while True:
        w = font.size(text[:i])[0]
        if w > width:
            # exceeded max width, search back for space
            pos = text.rfind(" ", 0, i - 1)
            if pos == -1:
                # no space in entire line, split it
                # a character before and add a '-'
                line = text[0:i - 2] + '-'
                rest = text[i - 2:]
            else:
                # Check if we are at the end of a sentence with a double space
                # if so, we need to remove both spaces
                end = text.find('.  ', pos - 1, pos + 2)
                if end != -1:
                    # end of sentence
                    line = text[0:end + 1]  # add 1 to include the '.'
                    rest = text[end + 3:]   # skip the 3 chars '.  '
                else:
                    # split at space
                    line = text[0:pos]      # up to the space
                    rest = text[pos + 1:]   # all after space
            break

        if i >= len(text):
            # ran out of text
            line = text
            rest = ''
            break

        if text[i - 1] == '\n':
            if text[i] == '\n':
                # double NL, paragraph break
                para = True
                line = text[0:i - 1] # before NLs
                rest = text[i + 1:]  # after NLs
            else:
                # single NL
                line = text[0:i - 1] # before NL
                rest = text[i:]      # after NL
            break

        i += 1

    line = line.rstrip()
    length = font.size(line)[0]
    return line, length, para, rest


def justify_text(rtext, pos, justify, vjustify):
    x, y = pos
    if justify == 'center':
        x = x - rtext.get_width() / 2
    elif justify == 'right':
        x = x - rtext.get_width()
    elif justify == 'left':
        pass
    else:
        logger.error('Bad value for justify (%s)', justify)
        raise ValueError

    if vjustify == 'center':
        y = y - rtext.get_height() / 2
    elif justify == 'bottom':
        y = y - rtext.get_height()
    elif justify == 'top':
        pass

    x = int(x)
    y = int(y)
    return (x, y)


def autoscale_text(surface, txt, pos, bold=True, color=colors.WHITE,
                   max_width=None, max_height=None,
                   font_size=14, min_font_size=10, font_name='freesans',
                   justify='left', vjustify='top'):
    # Display text at indicated position like text() below
    # auto automatically adjust the text size if the rendered text
    # if bigger then the max_width or max_height limites.  But never
    # use a smaller font then min_font_size
    if min_font_size > font_size:
        raise ValueError

    font_size = adj_font_size(surface, font_name, font_size, bold)
    min_font_size = adj_font_size(surface, font_name, min_font_size, bold)
    min_font_size = min(min_font_size, font_size)

    for fsize in range(font_size, min_font_size - 1, -1):
        font = pygame.font.SysFont(font_name, fsize, bold=bold, italic=False)
        rtext = font.render(txt, True, color)

        if max_width and rtext.get_width() > max_width:
            continue

        if max_height and rtext.get_height() > max_height:
            continue

        break

    pos = justify_text(rtext, pos, justify, vjustify)
    logger.debug('(%s, size=%s) -> %s', pos, fsize, txt)
    surface.blit(rtext, pos)
    return


def text(surface, txt, pos, bold=True, color=colors.WHITE,
         font_name='freesans', font_size=14,
         justify='left', vjustify='top'):
    # Display text at indicated position
    # x,y is position of the text
    # y is always the top of the text
    # if justify is left/right, will place text to the right/left of point
    # if vjustify if top/bottom, will place text below/above point.
    font_size = adj_font_size(surface, font_name, font_size, bold)
    font = pygame.font.SysFont(font_name, font_size, bold=bold, italic=False)
    rtext = font.render(txt, True, color)
    pos = justify_text(rtext, pos, justify, vjustify)
    logger.debug('(%s, size=%s) -> %s', pos, font_size, txt)
    surface.blit(rtext, pos)
    return


class TextBox:
    # Class for displaying text in a box (or rect)
    def __init__(self, surface, rect, xgap=10, ygap=4,
                 font_name='freesans', bold=True,
                 font_size=14, font_rsize=None, color=colors.WHITE):
        self.ymax = 0
        self.xmax = 0
        self.lines = []

        self.surface = surface
        self.color = color
        self.xgap = xgap
        self.ygap = ygap

        logger.debug('rect type is %s; rect=%s', type(rect), rect)
        if isinstance(rect, box.Box):
            logger.debug('Converting box to rect')
            self.rect = rect.rect()
            logger.debug('now rect is %s', self.rect)
        else:
            self.rect = rect

        # array of tuples of: x, y, text
        self.pos_lines = []

        if font_rsize:
            self.font_size = int(self.surface.get_height() * font_rsize)
            logger.debug('Set font size to %s from rsize of %s',
                         self.font_size, font_rsize)
        else:
            self.font_size = adj_font_size(surface, font_name, font_size, bold)
            logger.debug('Adjusted font_size from %s to %s',
                         font_size, self.font_size)

        self.font = pygame.font.SysFont(font_name, self.font_size,
                                        bold=bold,
                                        italic=False)
        return


    def name_values(self, items):
        # Display a list of name/value pairs
        # Note: that it is possible to go beyond the right edge of the box

        y = self.rect.top + self.ygap
        name_x = self.rect.left + self.xgap

        self.lines = []
        self.xmax = 0
        for name in items:
            value = str(items[name])

            name_width, name_height = _max_size(self.font, items.keys())
            value_width, value_height = _max_size(self.font,
                                                  map(str, items.values()))
            height = max(name_height, value_height)

            if y + height > self.rect.bottom:
                logger.debug('Ran out of room, unable to display all text')
                logger.debug('First line not displayed: "%s" "%s"',
                             name, value)
                break

            value_x = name_x + name_width + self.xgap
            right_x = value_x + value_width

            self.lines.append((name_x, y, name))
            self.lines.append((value_x, y, value))
            self.xmax = max(self.xmax, right_x)

            y = y + height

        self.ymax = y
        if self.xmax > self.rect.right:
            logger.error('some lines are were long (%d>%d)',
                         self.xmax, self.rect.right)

        return


    def line(self, text, line=1, justify='center'):
        # Display one line of text in box at given line.
        # if line is < 0, it is that number of lines from bottom (-1=bottom)
        t_width, t_height = self.font.size(text)
        y_start = self.rect.top + self.ygap
        y_end = self.rect.bottom - self.ygap
        lines = (y_end - y_start) / t_height
        lines = max(1, round(lines)) # always at least 1 line

        if line < 0:
            line = lines + (line + 1)

        y = y_start + (line - 1) * t_height

        if justify == 'center':
            x = self.rect.center[0] - t_width / 2
        elif justify == 'left':
            x = self.rect.left + self.xgap
        elif justify == 'right':
            x = self.rect.right - t_width - self.xgap

        self.xmax = x + t_width
        self.ymax = y + t_height

        logger.debug('line %s: "%s"', line, text)
        logger.debug('text size=%s, start=%s, end=%s, lines=%s, x=%s, y=%s',
                     (t_width, t_height), y_start, y_end, lines, x, y)

        self.lines.append((x, y, text))
        return


    def get_ymax(self):
        # Returns largest y value of text relative to rect
        return self.ymax - self.rect.top

    def get_xmax(self):
        # Returns largest x value of text relative to rect
        return self.xmax - self.rect.left


    def format(self, text, para_space=1):
        width = self.rect.width - self.xgap * 2
        y = self.rect.top + self.ygap
        x = self.rect.left + self.xgap
        bottom = self.rect.bottom - self.ygap

        font_height = self.font.size(text)[1]
        logger.debug('font_height=%s', font_height)
        max_len = 0
        self.lines = []

        while text:
            # If already beyond bottom end, just stop
            logger.debug('y=%s, y+height=%s, bottom=%s',
                         y, y + font_height, bottom)
            if y + font_height > bottom:
                logger.debug('Ran out of room, unable to display all text')
                logger.debug('Not displayed (len=%s): "%s"', len(text), text)
                break

            # find where to break line
            line, length, para, text = _break_text(text, self.font, width)
            max_len = max(max_len, length)

            logger.debug('line (%s, %s)  "%s"', x, y, line)
            self.lines.append((x, y, line))

            # If a paragraph, we may add additional space before next line
            if para:
                y += round(font_height * para_space)
            else:
                y += font_height

        self.ymax = y
        self.xmax = max_len

        return text


    def center(self, text):
        self.lines = []
        text_width, text_height = _max_size(self.font, [text])
        x = self.rect.center[0] - text_width / 2
        y = self.rect.center[1] - text_height / 2
        self.lines.append((x, y, text))
        self.ymax = y + text_height / 2
        self.xmax = x + text_width / 2
        return


    def display(self):
        for line in self.lines:
            logger.debug('displaying %s', line)
            x, y, txt = line
            image = self.font.render(txt, True, self.color)
            self.surface.blit(image, (int(x), int(y)))

        return


if __name__ == "__main__":
    # Test code
    import util

    def pause():
        # wait for a keypress (q=quit)
        key = None
        while not key:
            event = pygame.event.wait()
            if event.type == pygame.QUIT:
                key = pygame.K_q
            if event.type == pygame.KEYDOWN:
                key = event.key

        if key == pygame.K_q:
            sys.exit(0)

        return


    logger.init(None)

    colors.BLACK = (0, 0, 0)
    pygame.font.init()
    disp = pygame.display.set_mode(screen.DEF_SIZE)
    pygame.event.set_blocked(pygame.ACTIVEEVENT)

    rect = pygame.Rect(10, 10, 400, 400)
    box1 = box.Box((10, 10), (410, 410))
    dtext = TextBox(disp, rect, font_size=16)
    t = "* WHAT...Sub-freezing temperatures as low as 30 degrees expected.\n\n* WHERE...Portions of Maryland and Virginia between the Interstate\n81 and 95 corridors as well as the extreme eastern West\nVirginia panhandle.\n\n* WHEN...From midnight tonight to 9 AM EDT Friday.\n\n* IMPACTS...Frost and freeze conditions will kill crops, other\nsensitive vegetation and possibly damage unprotected outdoor   plumbing\nThe quick brown fox jumped over the lazy program.  Mary had a little lamb and it tasted very good." # noqa: E501
    dtext.format(t)

    disp.fill(colors.BLACK)
    box1.draw(disp, colors.WHITE, 1)
    dtext.display()
    pygame.display.update()
    pause()


    items1 = {
        'Item1:': 123,
        'Item2:': 23,
        'This is item 3:': 3003,
        'Time:': util.now(),
        '': '',
        'Another item:': 'I think this is a very very very long line, do you?',
        'line 7': 'booo',
    }
    dtext.name_values(items1)

    disp.fill(colors.BLACK)
    box1.draw(disp, colors.WHITE, 1)
    dtext.display()
    pygame.display.update()
    pause()


    box2 = box.Box((10, 10), (100, 50))
    btn = TextBox(disp, box2, font_size=12)
    btn.center('Click here')

    disp.fill(colors.BLACK)
    box2.draw(disp, colors.WHITE, 1)
    btn.display()
    pygame.display.update()
    pause()
