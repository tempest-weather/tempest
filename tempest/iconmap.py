#
# Icon map
#
# Copyright (c) 2020,2023 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
# SPDX-License-Identifier: GPL-3.0-or-later
#

import sys
import os
import jsonref
import pathlib
import importlib.resources as pkg_resources

# Local imports
from . import icons
from tempest.lib import util
from tempest.lib import logger
from tempest.lib import config
from tempest.lib import svg


def dir():
    return pkg_resources.files(icons)


def name2dir(name):
    if '/' in name:
        return pathlib.Path(name)

    return dir() / name


class IconMap:
    def __init__(self, idir):
        idir = name2dir(idir)
        self.icon_cache = {}
        self.mapfile_name = idir / 'iconmap.json'
        with open(self.mapfile_name) as fp:
            self.json = jsonref.load(fp)

        if self.json['dir'] != '':
            self.icon_dir = idir / self.json['dir']
        else:
            self.icon_dir = idir

        logger.debug('icon_dir set to %s', self.icon_dir)
        return


    def lookup(self, iname, night=False):
        # Lookup an icon file in the json icommap
        logger.debug('Looking up icon %s, night=%s', iname, night)
        try:
            icons = self.json['icons']
            icon = icons[iname]
            if night and icon['night_file']:
                fname = icon['night_file']
            else:
                fname = icon['file']
        except KeyError:
            fname = icons['unknown']['file']

        fname = '{}/{}'.format(self.icon_dir, fname)
        logger.debug('Returning %s', fname)
        return fname


    def load_from_cache(self, iname, night, size):
        # Implements a simple cache of the converted icons to help
        # with slight lag when switching screens.
        cache_id = '{}@@{}@@{}'.format(iname, night, size)
        if cache_id in self.icon_cache:
            logger.debug('Found %s in cache', cache_id)
            return self.icon_cache[cache_id]

        fname = self.lookup(iname, night)
        icon = svg.load_file(fname, size)
        self.icon_cache[cache_id] = icon
        logger.debug('Added %s to cache', cache_id)
        return icon


    def load(self, iname, size, night='current'):
        # Load the icon corresponding to the iname
        if str(night) == 'current':
            try:
                night = not util.is_daytime()
                logger.debug('night set to %s', night)
            except TypeError:
                night = False
                logger.error("Can't tell if it is daytime.  Assuming so")

        return self.load_from_cache(iname, night, size)


if __name__ == "__main__":
    # Test code
    logger.init(None, 'info')

    args = len(sys.argv) - 1
    if args == 1:
        idir = sys.argv[1]
    elif args == 0:
        idir = f'tempest/icons/{config.ICON_SET}'
    else:
        print('Usage {} [icondir]'.format(sys.argv[0]))
        sys.exit(1)

    im = IconMap(idir)

    print('Selected lookups...')
    print(' few      ', im.lookup('few'))
    print(' few night', im.lookup('few', night=True))
    print(' bkn      ', im.lookup('bkn'))
    print(' bkn night', im.lookup('bkn', night=True))
    print(' badname  ', im.lookup('badname'))


    def ckfile(d, icons, iname, ftype):
        global errors
        rslt = 'ok'

        try:
            fname = icons[iname][ftype]
            if fname is None:
                return

            path = '{}/{}'.format(d, fname)
            if not os.path.exists(path):
                rslt = 'MISSING'
                errors += 1

            print(' {:9s} {:15s} {:10s} {}'.format(rslt, iname, ftype, path))

        except KeyError:
            rslt = 'BAD ENTRY'
            print(' {:9s} {:15s} {:10s} ?'.format(rslt, iname, ftype))
            errors += 1

        return

    errors = 0
    print('Checking all entries...')
    icons = im.json['icons']
    for i in icons:
        ckfile(im.icon_dir, icons, i, 'file')
        ckfile(im.icon_dir, icons, i, 'night_file')

    print('Total errors:', errors)
