#
# Fetches select weather related data in another process
#
# Copyright (c) 2020,2023 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
# SPDX-License-Identifier: GPL-3.0-or-later
#

import os
import sys
import datetime
import logging
import signal
import errno
from threading import Event
from multiprocessing import Process

import psutil
import numpy as np
import pandas as pd
pd.options.mode.chained_assignment = 'raise'
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
#
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.ticker import MultipleLocator
# https://stackoverflow.com/questions/44262242/pyplot-plot-freezes-not-responding
# https://matplotlib.org/stable/users/explain/backends.html
import matplotlib
matplotlib.use('Agg')


# local imports
from tempest.lib import config
from tempest.lib import util
from tempest.lib import logger
from tempest import nws
from tempest import radar
from tempest import water
from tempest import screen


HOURLY_DAYS = 6


def freeze_value():
    # Return temperature that water freezes in the current UNITS
    if config.UNITS == 'us':
        return 32
    return 0


def generate_hourly_graph(df, day_num):
    # Generate hourly weather graphs for given day_num
    # day_num is the number of days from today
    # return the datetime of start of day

    this_day = util.now() + datetime.timedelta(days=day_num)

    logger.info('generating hourly graph for day %d', day_num)

    freezing = freeze_value()
    sunrise_dt = util.sunrise(this_day)
    sunset_dt = util.sunset(this_day)
    day_start_dt = util.get_start_of_day(this_day)
    day_end_dt = util.get_end_of_day(this_day)
    next_day_start_dt = day_end_dt + datetime.timedelta(hours=1)
    day_start_ts = pd.Timestamp(day_start_dt)
    day_end_ts = pd.Timestamp(day_end_dt)
    next_day_start_ts = pd.Timestamp(next_day_start_dt)
    day_string = util.get_day_string(this_day)

    logger.debug('day start %s', day_start_dt)
    logger.debug('day end   %s', day_end_dt)

    # Reduce the amount of data to just the desired time period
    # Note: we add 1 hr to the end of the slice so we get 2400 hrs
    # (0000 next day) on the graphs

    df = df[day_start_ts:next_day_start_ts].copy()

    #df['Time'] = pd.to_datetime(df.index)
    logger.debug('df after slice\n%s', util.df_indent(df))

    plt.style.use('dark_background')
    fig, axs = plt.subplots(3, 1, sharex=True)

    # Note: had to use axs[].plot instead of df.plot in order to get
    # the major locator to work properly

    # Do we need to make room for a right-side y-axis?
    right_axis = False
    snow_max = df['Snow'].max()
    if snow_max is not np.NaN and snow_max > 0:
        right_axis = True

    if right_axis is True:
        bbox = (1.09, 1)
    else:
        bbox = (1.04, 1)

    # Temperature
    ymin = min(df['Temp'].min(), df['Feel'].min(), df['Dew'].min())
    ymax = max(df['Temp'].max(), df['Feel'].max(), df['Dew'].max())
    axs[0].set(ylabel='temperature',
               title='Hourly Weather Data For {}'.format(day_string))
    axs[0].plot(df.index, df['Feel'], color='violet', label='Feel')
    axs[0].plot(df.index, df['Temp'], color='red', label='Temp')
    axs[0].plot(df.index, df['Dew'], color='limegreen', label='Dew')
    axs[0].hlines(freezing, day_start_ts, day_end_ts, color='b')
    axs[0].yaxis.set_minor_locator(MultipleLocator(5))
    # So y axis starts/ends on a multiple of 10 (or 5)
    logger.debug('ymin=%s, ymax=%s', ymin, ymax)
    if ymin is not np.NaN and ymax is not np.NaN:
        if config.UNITS == 'us':
            axs[0].set_ylim(util.floor(ymin, 10), util.ceil(ymax, 10))
        else: # assuming 'si' (centigrade)
            axs[0].set_ylim(util.floor(ymin, 5), util.ceil(ymax, 5))
    axs[0].legend(bbox_to_anchor=bbox, loc='upper left')

    # Precipitation probability and cloud cover
    axs[1].set(ylabel='percent')
    if not df.empty:
        axs[1].fill_between(df.index, df['Skycover'], facecolor='0.6',
                            label='Cloud', alpha=1)
        axs[1].fill_between(df.index, df['Precip'], facecolor='limegreen',
                            label='Precip', alpha=1)
    axs[1].plot(df.index, df['Humid'], color='red', label='Humid')
    # we don't have info on snow/mix precip :-(
    axs[1].set_ylim(0, 100)
    axs[1].yaxis.set_major_locator(MultipleLocator(25))

    if snow_max is not np.NaN and snow_max > 0:
        ax1b = axs[1].twinx()
        ax1b.set_ylabel('inches')
        ax1b.plot(df.index, df['Snow'], color='royalblue', label='Snow/hr')
        ax1b.tick_params(axis='y')
        ax1b.set_ylim(0, util.ceil(snow_max, 1))

        # Merge legends from both y axis
        lines1, labels1 = axs[1].get_legend_handles_labels()
        lines2, labels2 = ax1b.get_legend_handles_labels()
        axs[1].legend(lines1 + lines2, labels1 + labels2,
                      bbox_to_anchor=bbox, loc='upper left')
    else:
        axs[1].legend(bbox_to_anchor=bbox, loc='upper left')

    # Wind and gusts
    ymax = max(df['Gust'].max(), df['Wind'].max())
    # XXX disabled for now, not working, too agressive
    # # Replace wind gusts with NaN if they don't meet the definition.
    # gust_min, gust_delta = util.get_gust_values()
    # logger.debug('gust limits: %s %s', gust_min, gust_delta)
    # # Filter out gusts below gust_min
    # df.loc[(df.Gust < gust_min), 'Gust'] = np.NaN
    # # Filter out gusts below gust_delta between gust and nominal speed
    # df.loc[(df.Gust - df.Wind < gust_delta), 'Gust'] = np.NaN
    # logger.debug('df after Gust adjustment\n%s', util.df_indent(df))

    axs[2].plot(df.index, df['Gust'], color='blue', label='Gust')
    axs[2].plot(df.index, df['Wind'], color='deepskyblue', label='Wind')
    axs[2].set(ylabel='speed')
    if ymax is not np.NaN:
        axs[2].set_ylim(0, util.ceil(ymax, 10))
    axs[2].legend(bbox_to_anchor=bbox, loc='upper left')

    mhours = mdates.HourLocator(interval=2)
    mhours_fmt = mdates.DateFormatter('%H00')
    mhours_fmt.set_tzinfo(util.tz())
    logger.debug('mhours is %s', mhours)

    # Common stuff for all plots
    for ax in axs:
        # Lighten background for daylight hours
        ax.axvspan(sunrise_dt, sunset_dt, facecolor='0.9', alpha=0.2)
        ax.xaxis.set_major_locator(mhours)
        ax.xaxis.set_major_formatter(mhours_fmt)
        ax.set_xlim(day_start_ts, day_end_ts)
        ax.grid(True)

    fig.tight_layout()
    # Allow room for legends on right side
    fig.subplots_adjust(right=0.85)
    fig.align_ylabels(axs)

    dpi = fig.get_dpi()
    logger.debug('dpi %d', dpi)
    # This ends up a wee bit smaller then the actual screen size,
    # but that works our great as it allows up to add a border
    # when displaying it.
    fig.set_size_inches(screen.Screen.size[0] / dpi,
                        screen.Screen.size[1] / dpi)

    # enable this to see graphs as they are generated
    #plt.show()

    try:
        os.makedirs(config.hourly_dir, exist_ok=True)
    except OSError as err:
        if err.errno != errno.EEXIST:
            raise

    logger.info('Saving hourly image for day %d', day_num)
    final_name = '{}/day_{}.png'.format(config.hourly_dir, day_num)
    temp_name = final_name + '.part'
    logger.debug('saving to %s', temp_name)
    plt.savefig(temp_name, bbox_inches='tight', dpi=dpi, format='png')
    logger.debug('renaming %s to %s', temp_name, final_name)
    os.rename(temp_name, final_name)
    logger.debug('done')

    fig.clf()
    plt.close('all')

    return day_start_dt


def generate_hourly_graphs(df):
    # Generates hourly weather graphs for each day we have
    # hourly data

    hourly_map = {}

    # limit to 4 days as that is all we display
    for day in range(HOURLY_DAYS):
        dt = generate_hourly_graph(df, day)
        hourly_map[day] = dt

    logger.debug('Saving hourly-map: %s', hourly_map)
    util.save_hmap('hourly-map.json', hourly_map)
    return


def invalidate_hourly_graphs():
    for day in range(HOURLY_DAYS):
        f = '{}/day_{}.png'.format(config.hourly_dir, day)
        logger.debug('Removing %s', f)
        try:
            os.remove(f)
        except FileNotFoundError:
            pass


def need_hourly_update(dt):
    if dt is None:
        logger.debug('dt is None, do nothing')
        return False

    for day in range(HOURLY_DAYS):
        fname = '{}/day_{}.png'.format(config.hourly_dir, day)
        day_dt = util.date_from_file(fname)
        logger.debug('dt    = %s', dt)
        logger.debug('day_dt= %s', day_dt)
        if day_dt is None:
            return True
        if dt > day_dt:
            return True

    return False


#####################################################################
class DataFetch:
    # Class for fetching weather data

    proc = None
    child_pid = None
    term_count = 0
    done = None # Set for real in child by fetcher()
    nws = None
    radar = None
    water = None


    def signal_handler(self, sig, unused_frame):
        logger.debug('pid %d received signal: %s (%d)',
                     os.getpid(), signal.Signals(sig).name, sig)

        if sig == signal.SIGTERM:
            self.done.set()

            self.term_count += 1
            if self.term_count > 1:
                logger.debug('Got another SIGTERM (count=%d), exiting now',
                             self.term_count)
                sys.exit(2)

        return


    def validate_data(self):
        cfg_file = 'lastcfg.json' # relative to CACHE_DIR

        # Important config values that determine what gets downloaded.
        # No need to include radar sites as they always have their own
        # unique filename, but that will leave old radar downloads to persist.
        cur_config = {'units': config.UNITS,
                      'lat/lon': '{},{}'.format(config.LAT, config.LON),
                      'water': '{}'.format(config.WATER_SITE)}

        try:
            old_config = util.load_data(cfg_file)
        except FileNotFoundError:
            logger.debug('File %s not found', cfg_file)
            old_config = None

        if cur_config != old_config:
            logger.info('Config changed, invalidating downloaded data')
            self.nws.invalidate()
            self.water.invalidate()
            invalidate_hourly_graphs()
            util.save_data(cfg_file, cur_config)
        else:
            logger.info('Config unchanged')

        return


    def fetcher_test(self):
        # Reduce logging noise from mathplotlib
        mpl_logger = logging.getLogger('matplotlib')
        mpl_logger.setLevel(logging.WARNING)

        self.nws = nws.Nws(config.LAT, config.LON, config.CACHE_DIR)
        self.nws.fetch()

        self.radars = radar.Radars(config.RADAR_SITES)
        self.radars.fetch()

        self.water = water.Water()
        self.water.fetch()

        generate_hourly_graphs(self.nws.get_hourly_dataframe())
        return


    def fetcher(self):
        # Reduce logging noise from mathplotlib
        mpl_logger = logging.getLogger('matplotlib')
        mpl_logger.setLevel(logging.WARNING)

        ppid = os.getppid()

        self.nws = nws.Nws(config.LAT, config.LON, config.CACHE_DIR)
        self.radars = radar.Radars(config.RADAR_SITES)
        self.water = water.Water()

        self.validate_data()

        # Do an initial load attempt so we don't always fetch at startup
        self.nws.load()

        # Run till we get a signal (handler will set the done event)
        while not self.done.is_set():
            if os.getppid() == 1:
                logger.error('Exiting, parent died (%d)', ppid)
                return

            if self.nws.need():
                self.nws.fetch()

            if need_hourly_update(self.nws.get_hourly_dataframe_time()):
                generate_hourly_graphs(self.nws.get_hourly_dataframe())

            if self.radars.need():
                # if temperature is below the configed limit get the
                # rain/snow version of the radar
                snow = False
                temp = self.nws.current.temperature
                if temp is not None:
                    snow = temp < config.RADAR_RAIN_SNOW

                logger.debug('temp=%s, snow=%s', temp, snow)
                self.radars.fetch(rainsnow=snow)

            if self.water.need():
                self.water.fetch()

            self.done.wait(300)

        logger.info('done event set, exiting')
        return


    def fetcher_child(self, unblock_sigs):
        # Start of child process for the data fetcher
        self.done = Event()

        logger.init('datafetch')
        logger.info('starting up, PID is %d', os.getpid())

        signal.signal(signal.SIGTERM, self.signal_handler)

        if unblock_sigs:
            logger.info('Unblocking %s', unblock_sigs)
            signal.pthread_sigmask(signal.SIG_UNBLOCK, unblock_sigs)

        try:
            self.fetcher()
        except Exception:
            logger.critical('Got unhandled exception', exc_info=True)
            os._exit(3)

        return

    #
    # The rest of these functions are called by the parent/main process
    #

    def start(self, unblock_sigs):
        # Starts the child process
        logger.debug('about to start child process')
        args = (unblock_sigs, )
        self.proc = Process(target=self.fetcher_child, args=args)
        self.proc.start()
        self.child_pid = self.proc.pid
        logger.debug('child_pid = %s', self.child_pid)
        return


    def is_alive(self):
        # Returns True if the child process is alive, else False

        self.proc.join(0)

        # This doesn't this work
        #alive = self.proc.is_alive()
        #logger.debug('is_alive=%s, pid_exists=%s', alive, exists)
        #return alive

        exists = psutil.pid_exists(self.child_pid)
        return exists


    def stop(self, force=0):
        # Stops the child process
        if not self.proc:
            logger.debug('no process')
            return

        if not self.proc.is_alive():
            logger.debug('process not alive')

        logger.debug("doing terminate")
        self.proc.terminate()

        if force:
            # we don't want to wait, a 2nd SIGTERM will cause
            # the child to exit from the signal handler
            self.proc.join(1)
            logger.debug("doing terminate again")
            self.proc.terminate()

        logger.debug("doing join")
        self.proc.join()
        return


if __name__ == "__main__":
    logger.init(None)
    config.hourly_dir = '{}/hourly'.format(config.CACHE_DIR)
    fetch = DataFetch()
    fetch.fetcher_test()
