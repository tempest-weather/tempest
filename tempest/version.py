#
# Copyright (c) 2020,2023 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
# SPDX-License-Identifier: GPL-3.0-or-later
#

import os
import pygame

# read version from installed package
from importlib.metadata import version
from importlib.metadata import metadata

# local imports
from tempest.lib import logger


md = metadata(__package__)

prog = md['Name'].title()      # pyproject's "name"
version = version(__package__) # pyproject's "version"
full_name = md['Summary']      # pyproject's "description"
proj_url = md['Project-URL']   # pyproject's "repository"
group_url = md['home-page']    # pyproject's "homepage"


# Log some startup info
def info():
    logger.info('%s, %s', prog, version)
    logger.info('pygame version %s, SDL version %s',
                pygame.version.ver, pygame.get_sdl_version())
    logger.info('PID: %d', os.getpid())
    return
