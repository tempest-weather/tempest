#!/usr/bin/env python3
#
# Display a bunch of icons
#
# Copyright (c) 2020,2023 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
# SPDX-License-Identifier: GPL-3.0-or-later
#

import os
import sys
import math

import pygame

# local imports
from tempestlib import colors
from tempestlib import svg


def pause():
    # wait for a keypress (q=quit)
    key = None
    while not key:
        event = pygame.event.wait()
        if event.type == pygame.QUIT:
            key = pygame.K_q
        if event.type == pygame.KEYDOWN:
            key = event.key

    if key == pygame.K_q:
        sys.exit(0)

    return


def add_file(fname):
    # Adds fname to files[] if it has a .svg suffix
    global files
    ext = os.path.splitext(fname)[1]
    if ext != '.svg':
        print('Skipping', fname)
        return

    files.append(fname)
    return


def add_dir(dname):
    # Adds files in dname to files[] if they have a .svg suffix
    lst = os.listdir(dname)
    lst.sort()
    for f in lst:
        fname = dname + '/' + f
        if os.path.isdir(fname):
            add_dir(fname)
        else:
            add_file(fname)
    return


# Get the list of files to load
files = []
prog = sys.argv.pop(0)
for fname in sys.argv:
    if os.path.isdir(fname):
        add_dir(fname)
    else:
        add_file(fname)

if files == []:
    print('No svg files found')
    print('Usage: {} <dirs-or-files>'.format(prog))
    sys.exit(1)

num = len(files)
rows = int(math.sqrt(num))
cols = int(math.sqrt(num) + 1)

font_size = 10
img_size = 64

pygame.font.init()
font = pygame.font.SysFont('freesans', font_size, bold=True, italic=False)

text_height = font.render('X', True, colors.WHITE).get_height()

x_size = 2 * img_size # 2x so we have more room for filename
y_size = img_size + text_height
screen_size = (x_size * cols, y_size * rows)

disp = pygame.display.set_mode(screen_size)
pygame.event.set_blocked(pygame.ACTIVEEVENT)
disp.fill(colors.BLACK)

x = 0
y = 0
for fname in files:
    print('Loading file {}'.format(fname))
    try:
        img = svg.load_file(fname, img_size)
        disp.blit(img, (x, y))
    except (FileNotFoundError, IsADirectoryError):
        print('{}, file not found or is a directory'.format(fname))
        continue
    except Exception as e:
        print('Failed to load file {}.  Reason: {}'.format(fname, e))
        continue

    rtext = font.render(os.path.basename(fname), True, colors.WHITE)
    disp.blit(rtext, (x, y + img_size))

    x += x_size
    if x > screen_size[0] - x_size:
        y += y_size
        x = 0

pygame.display.update()
print('Waiting for a key press in pygame window to exit')
pause()
