#
# Copyright (c) 2020,2023 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
# SPDX-License-Identifier: GPL-3.0-or-later
#

import requests
from cachecontrol import CacheControl
from cachecontrol.caches.file_cache import FileCache

# Local imports
from tempest.lib import logger
from tempest.lib import util


def merge_headers(default, more):
    if default is None:
        return more

    if more is None:
        return default

    hdr = default.copy()
    hdr.update(more)
    return hdr


class Url:
    def __init__(self, cache, headers=None):
        # Create a Url object.
        # Cache is location (directory) for cache.  It will be created if
        # needed.
        # headers is a default set of headers to be included in each request.
        self.cache = cache
        self.headers = headers

        self.session = CacheControl(requests.Session(),
                                    cache=FileCache(self.cache))
        return


    def get(self, url, params=None, headers=None, timeout=15):
        # Fetch a URL
        # Add header to any default set of headers.  If a header is duplicated,
        # this version overrides the one from __init__.
        # timeout is the connection and read timeouts

        headers = merge_headers(self.headers, headers)
        logger.debug('fetching "%s", timeout=%d', url, timeout)

        try:
            r = self.session.get(url, params=params,
                                 headers=headers, timeout=timeout)
        except (requests.exceptions.ConnectionError,
                requests.exceptions.ReadTimeout,
                requests.exceptions.TooManyRedirects,
                requests.exceptions.HTTPError) as e:
            logger.error('Error: Failed to download %s (timeout=%d)',
                         url, timeout)
            logger.error('Error: request parameters: %s', params)
            logger.error('Error: request headers: %s', headers)
            logger.error('Error: %s', e)
            return False

        logger.debug('HTTP status is %d, %s', r.status_code, r.reason)
        logger.debug('from cache: %s', r.from_cache)
        logger.debug('request parameters: %s', util.dict_indent(params))
        logger.debug('request headers: %s', util.dict_indent(headers))
        logger.debug('response headers: %s', util.dict_indent(r.headers))

        #r.raise_for_status()
        if r.status_code != requests.codes.ok:
            logger.error('Error: Failed to download %s', url)
            return False

        return r


    def download(self, url, fname, params=None, headers=None, timeout=15):
        result = self.get(url, params=params, headers=headers, timeout=timeout)

        if not result:
            logger.error('Failed to download %s', url)
            return False

        with open(fname, 'wb') as fp:
            fp.write(result.content)

        return True
