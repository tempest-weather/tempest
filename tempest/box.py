#
# Class for creating a box and accessing various attributes of it
#
# Copyright (c) 2020,2023 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
# SPDX-License-Identifier: GPL-3.0-or-later
#

import pygame

# Local imports
from tempest.lib import logger


# pylint: disable=invalid-name
# index of x and y values in points ul and lr
x = 0
y = 1


def contains_list(pos, blist):
    # Return the first box in list that contains the given position
    for b in blist:
        if b.contains(pos):
            return b

    return None


def adj_pos_for_width(disp, pos, width):
    # Adjust position when at edge of screen to fit the desired line width

    xmax, ymax = disp.get_size()
    if width == 1:
        return pos
    (x, y) = pos
    if x == 0:
        x = width / 2
    if y == 0:
        y = width / 2
    if x == xmax:
        x -= width / 2
    if y == ymax:
        y -= width / 2
    return (int(x), int(y))


def draw_line(disp, color, a, b, width):
    # Draw a line from a to b of given width
    a = adj_pos_for_width(disp, a, width)
    b = adj_pos_for_width(disp, b, width)
    #logger.debug('Drawing line: %s %s width=%s', a, b, width)
    pygame.draw.line(disp, color, a, b, width)
    return


class Box:
    """
    Create and box and access various attributes of it

            UL    top(y)    UR
            +---------------+
            |               |
     left(x)|     center    |right(x)
            |               |
            +---------------+
            LL  bottom(y)  LR

    Note: numerically, top is less then bottom and left is less than right
    """
    ul = (0, 0)
    lr = (0, 0)

    def __init__(self, UL, LR, name=None): # noqa: N803
        """Create a box with given upper-left and lower-right positions (x,y)"""
        self.ul = (int(UL[0]), int(UL[1]))
        self.lr = (int(LR[0]), int(LR[1]))
        self.name = name
        logger.debug('Created box %s at %s, %s', name, self.ul, self.lr)
        return

    def UL(self): # noqa: N802
        """Upper-left position"""
        return self.ul

    def LR(self): # noqa: N802
        """Lower right position"""
        return self.lr

    def LL(self): # noqa: N802
        """Lower-left position"""
        return (self.ul[x], self.lr[y])

    def UR(self): # noqa: N802
        """Upper-right position"""
        return (self.lr[x], self.ul[y])

    def left(self):
        """Left edge (x) of box"""
        return self.ul[x]

    def right(self):
        """Right edge (x) of box"""
        return self.lr[x]

    def top(self):
        """Top edge (y) of box"""
        return self.ul[y]

    def bottom(self):
        """Bottom edge (y) of box"""
        return self.lr[y]

    def width(self):
        """Width (x) of box"""
        return self.right() - self.left()

    def height(self):
        """Height (y) of box"""
        return self.bottom() - self.top()

    def center_x(self):
        """Position of x value of center of box"""
        return int(self.left() + self.width() / 2)

    def center_y(self):
        """Position of y value of center of box"""
        return int(self.top() + self.height() / 2)

    def contains(self, pos):
        """Return True is pos in contained inside box (exclusive of edges)"""
        if pos[x] <= self.left() or pos[x] >= self.right():
            return False
        if pos[y] <= self.top() or pos[y] >= self.bottom():
            return False
        return True

    def draw(self, disp, color, width=1):
        #logger.debug('Drawing box %s', self)
        draw_line(disp, color, self.LL(), self.LR(), width)
        draw_line(disp, color, self.LR(), self.UR(), width)
        draw_line(disp, color, self.UR(), self.UL(), width)
        draw_line(disp, color, self.UL(), self.LL(), width)
        return

    def rect(self):
        # returns a pygame.Rect corresponding to the box
        return pygame.Rect(self.left(), self.top(),
                           self.width(), self.height())

    def __repr__(self):
        s = ''
        if self.__class__.__module__ != "__main__":
            s = '{}.'.format(self.__class__.__module__)

        s += '{}(UL={}, LR={}, name={})'.format(self.__class__.__qualname__,
                                                self.ul, self.lr, self.name)
        return s

    def __str__(self):
        return 'Box({}, {}, {})'.format(self.ul, self.lr, self.name)


if __name__ == "__main__":
    b = Box((10, 12), (50, 60))
    print('box:       ', b)
    print('left:      ', b.left())
    print('right:     ', b.right())
    print('top:       ', b.top())
    print('bottom:    ', b.bottom())
    print('height:    ', b.height())
    print('width:     ', b.width())
    print('centerx:   ', b.center_x())
    print('centery:   ', b.center_y())
    print('repr:      ', repr(b))
    print('str:       ', str(b))
    print('eval(repr):', eval(repr((b)))) #pylint: disable=eval-used
    print()
    print('contains(2,20)no:   ', b.contains((2, 20)))
    print('contains(11,20)yes: ', b.contains((11, 20)))
    print('contains(21,59)yes: ', b.contains((21, 59)))
    print('contains(21,60)no:  ', b.contains((21, 60)))
    print('contains(55,20)no:  ', b.contains((55, 20)))
