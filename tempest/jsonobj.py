#
# Copyright (c) 2020,2023 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
# SPDX-License-Identifier: GPL-3.0-or-later OR MIT
#
# DataPoint class came from
# https://github.com/lukaskubis/darkskylib which is under the MIT license
#

import os
import time
import json

# local imports
from tempest.lib import logger


class DataPoint:
    def __init__(self, data):
        self._data = data

        if isinstance(self._data, dict):
            for name, val in self._data.items():
                setattr(self, name, val)

        if isinstance(self._data, list):
            setattr(self, 'data', self._data)

    def __setattr__(self, name, val):
        def setval(new_val=None):
            return object.__setattr__(self, name, new_val if new_val else val)

        if not isinstance(val, (list, dict)) or name == '_data':
            return setval()

        if '@' in name:
            name = name.replace('@', '_')

        if isinstance(val, list):
            val = [DataPoint(v) if isinstance(v, dict) else v for v in val]
            return setval(val)

        setval(DataPoint(val))

    def __getitem__(self, key):
        return self._data[key]

    def __len__(self):
        return len(self._data)

    def __repr__(self):
        s = ''
        if self.__class__.__module__ != "__main__":
            s = '{}.'.format(self.__class__.__module__)

        s += '{}({})'.format(self.__class__.__qualname__,
                             self._data.keys())
        return s


class JsonObj(DataPoint):
    pass


class Json:
    # Json class holds various aspects related to some json
    # It includes
    #   time        time loaded/set
    #   json        actual json
    #   file_name   used to save/load
    #   obj         JsonObj of the json
    #
    # With this sample json
    # {
    #     "properties": {
    #         "periods": [
    #             {
    #                 "temperature": 65
    #             },
    #         ],
    #     },
    # }
    #
    # The 'obj' allows references to the json like
    #   obj.properties.periods[0].temperature
    # whereas with 'json', you need to specify
    #   json['properties']['periods'][0]['temperature']
    #

    def __init__(self, file_name):
        self.time = 0     # timestamp when objest is set/loaded
        self.json = None
        self.obj = None
        self.file_name = file_name
        self.saved = False
        return

    def load(self):
        # load the json from the file
        try:
            logger.info('Reloading %s', self.file_name)
            with open(self.file_name) as fp:
                self.json = json.load(fp)
                self.time = time.time()
                self.obj = JsonObj(self.json)
                self.saved = True
                return True

        except FileNotFoundError:
            logger.error('File not found: %s', self.file_name)
        except Exception:
            logger.error('Unknown error loading %s',
                         self.file_name, exc_info=True)

        return False


    def need_reload(self):
        if not os.path.exists(self.file_name):
            logger.error('Cannot load %s, file does not exist', self.file_name)
            return False

        mtime = os.path.getmtime(self.file_name)
        if mtime > self.time:
            logger.info('Reloading of %s needed', self.file_name)
            return True

        return False


    def set(self, json_data):
        # Set json to contents of json_data arguemnt
        if self.json != json_data:
            self.json = json_data
            self.time = time.time()
            self.obj = JsonObj(self.json)
            self.saved = False
        else:
            logger.debug('Json identical to what we already had')

        return


    def save(self):
        # Save json to file.  Use a temp name so any reader doesn't
        # see a partially written file.

        if self.json is None:
            logger.warning('Cannot save %s, nothing loaded/set yet',
                           self.file_name)
            return False

        if self.saved is True:
            logger.debug('No need to save %s, not changed', self.file_name)
            return True

        logger.debug('Saving %s', self.file_name)
        tmp = self.file_name + '.part'
        with open(tmp, 'w') as fp:
            json.dump(self.json, fp)

        os.rename(tmp, self.file_name)
        return True


    def remove(self):
        try:
            logger.debug('Removing %s', self.file_name)
            os.remove(self.file_name)
        except FileNotFoundError:
            pass
        return
