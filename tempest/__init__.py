#
# Copyright (c) 2022-2023 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
# This software is licensed according to the included LICENSE file
# SPDX-License-Identifier: GPL-3.0-or-later
#


# Constants defining types of box styles
BOX_STYLE_DAILY = 1
BOX_STYLE_TITLE = 2
BOX_STYLE_OUTLINE = 3
BOX_STYLE_ALERTS = 4

# Font info
DEF_FONT_NAME = 'freesans'
