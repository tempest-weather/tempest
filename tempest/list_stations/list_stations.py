#!/usr/bin/env python3
#
# Fetch list of possible observation stations
#
# Copyright (c) 2020,2023 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
# SPDX-License-Identifier: GPL-3.0-or-later
#

import os
import sys
import traceback
from types import SimpleNamespace

from geographiclib.geodesic import Geodesic
import requests
import geopy.distance


def get_bearing(pos1, pos2):
    brng = Geodesic.WGS84.Inverse(pos1[0], pos1[1], pos2[0], pos2[1])['azi1']
    if brng < 0:
        return 365 + brng
    return brng


def handle_error(r):
    if r.status_code != requests.codes.ok:
        print('HTTP error {}, {}'.format(r.status_code, r.reason))
        try:
            print('detail: {}'.format(r.json()['detail']))
        except Exception:
            pass
        sys.exit(2)
    return


def main():
    global created_config

    # Try importing local config.py
    try:
        from tempestlib import config
        lat_lon_source = 'config.py'
        lat = config.LAT
        lon = config.LON
    except (ImportError, ModuleNotFoundError):
        print('A minimal src/tempestlib/config.py is needed for')
        print('this program to run.  It just needs one line:')
        print("UNITS = 'us'")
        sys.exit(1)

    # other local imports
    from tempestlib import util
    from tempestlib import units

    try:
        if len(sys.argv) == 3:
            lat = sys.argv[1]
            lon = sys.argv[2]
            lat_lon_source = 'command line'
        elif len(sys.argv) == 2:
            lat, lon = sys.argv[1].split(',')
            lat_lon_source = 'command line'
        elif len(sys.argv) != 1:
            raise ValueError

        lat = float(lat)
        lon = float(lon)

    except (ValueError, TypeError):
        if len(sys.argv) == 1:
            print('No lat/lon found in config.py (or file does not exist)')
            print("You'll need to specify lat/lon on command line")
        else:
            print('Bad arguments')

        print('Usage: {} <lat> <long>'.format(sys.argv[0]))
        print('       {} <lat>,<long>'.format(sys.argv[0]))
        print('       {}               (read from config.py)'.
              format(sys.argv[0]))
        return False


    # Get elevation
    params = {
        'x': lon,
        'y': lat,
        'units': 'Feet',
        'output': 'json',
    }

    try:
        print('trying to get elevation...')
        r = requests.get('https://nationalmap.gov/epqs/pqs.php', params)
        json = r.json()
        query = json['USGS_Elevation_Point_Query_Service']['Elevation_Query']
        my_elevation = units.convert(query['Elevation'], query['Units'])
        my_elevation = round(query['Elevation'])
    except Exception:
        my_elevation = None


    # Get NWS points
    print('trying to get NWS point info...')
    url = 'https://api.weather.gov/points/{},{}'.format(lat, lon)
    r = requests.get(url)
    handle_error(r)
    json = r.json()
    zone_url = json['properties']['forecastZone']

    # Get Zone info
    print('trying to get NWS zone info...')
    r = requests.get(zone_url)
    handle_error(r)
    json = r.json()
    my_zone_id = json['properties']['id']


    # Get all stations in this zone
    stations = []
    for station in json['properties']['observationStations']:
        name = os.path.basename(station)
        print('trying to get NWS station info for {}...'.format(name))
        r = requests.get(station)
        handle_error(r)
        json = r.json()
        pos = (json['geometry']['coordinates'][1],
               json['geometry']['coordinates'][0])

        elevation = json['properties']['elevation']['value']
        uom = json['properties']['elevation']['unitCode']
        elevation = units.convert(elevation, uom)

        # you can change miles to km if you want
        distance = geopy.distance.distance((lat, lon), pos).miles
        distance = units.convert(distance, 'miles')
        #distance = round(geopy.distance.distance((lat, lon), pos).miles)

        this = SimpleNamespace()
        this.id = json['properties']['stationIdentifier']
        this.name = json['properties']['name']
        this.pos = pos
        this.distance = distance
        this.elevation = elevation
        this.bearing = round(get_bearing((lat, lon), pos))

        stations.append(this)

    # Sort by distance
    stations = sorted(stations, key=lambda s: s.distance)

    print('=======================================')
    print('Your Lat/lon:   {}, {} (from {})'.format(lat, lon, lat_lon_source))
    print('Your elevation: {}'.format(units.pretty(my_elevation, 'Elevation')))
    print('Your zone:      {}'.format(my_zone_id))
    print('')

    print('Choose one or more of these for your stations (sorted by distance):')
    for station in stations:
        print('  {} - {}'.format(station.id, station.name))
        print('         Location:  {}, {}'.format(station.pos[0],
                                                  station.pos[1]))
        print('         Distance:  {}'.format(units.pretty(station.distance,
                                                           'Distance')))
        rose = util.deg_to_compass(station.bearing)
        print('         Bearing:   {} ({} deg)'.format(rose, station.bearing))
        print('         Elevation: {}'.format(units.pretty(station.elevation,
                                                           'Elevation')))
    # end main()
    return True


def startup():
    try:
        main()
    except Exception:
        traceback.print_exc()

if __name__ == '__main__':
    startup()
