#
# Copyright (c) 2020,2023 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
# SPDX-License-Identifier: GPL-3.0-or-later
#

import os

os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide"
import pygame


# local imports
from tempest.lib import config
from tempest.lib import logger
from tempest.lib import colors
from tempest import version
from tempest import disptext


# Default screen (window) size
DEF_SIZE = (800, 480)


class Screen:
    # There can only be one screen, so we do it all at the class level

    # Default size that matches the target touchscreen
    size = DEF_SIZE
    flags = pygame.RESIZABLE

    @classmethod
    def init(cls):
        pygame.display.init()
        pygame.font.init()

        if config.FULLSCREEN == 'auto':
            if pygame.display.Info().current_w <= 1024 and \
               pygame.display.Info().current_h <= 1024:
                config.FULLSCREEN = True
            else:
                config.FULLSCREEN = False

        if config.FULLSCREEN:
            cls.flags = pygame.FULLSCREEN
            cls.size = (pygame.display.Info().current_w,
                        pygame.display.Info().current_h)

        pygame.display.set_caption(version.full_name)
        logger.debug("Using screen size: %s, flags=%s", cls.size, cls.flags)
        cls.surface = pygame.display.set_mode(cls.size, cls.flags)

        if config.FULLSCREEN:
            pygame.mouse.set_visible(0)

        if not config.FULLSCREEN:
            try:
                logger.debug('Telling pygame to allow screensavers')
                pygame.display.set_allow_screensaver(True)
            except AttributeError:
                # This does not exist yet in pygame 1.9.6
                # https://github.com/pygame/pygame/issues/1707#event-3367880766
                logger.info('Unable to allow screensaver to run with this '
                            'version of pygame')
                pass

        return

    @classmethod
    def simple_message(cls, msg):
        cls.surface.fill(colors.BLACK)
        x, y = cls.size
        pos = (x / 2, y * 0.35) # bit higher then center of screen
        disptext.text(cls.surface, msg, pos,
                      font_size=30, justify='center')
        logger.debug('Displaying message: %s', msg)
        pygame.display.update()
        return

    @classmethod
    def splash(cls):
        cls.simple_message(f'{version.prog} is starting up...')
        return

    @classmethod
    def exiting(cls):
        cls.simple_message(f'{version.prog} is exiting...')
        return

    @classmethod
    def close(cls):
        pygame.quit()
        pygame.surface = None
        return

    @classmethod
    def set_size(cls, size):
        cls.size = size
        pygame.display.set_caption('{} {}'.format(version.full_name, cls.size))
        logger.debug('New size %s, flags=%s', cls.size, cls.flags)
        cls.surface = pygame.display.set_mode(cls.size, cls.flags)
        return


if __name__ == "__main__":
    # Test code
    import time
    logger.init(None)
    Screen.init()
    Screen.splash()
    time.sleep(1)
    Screen.exiting()
    time.sleep(1)
