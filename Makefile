#
# This software is licensed according to the included LICENSE file
#
# Copyright (c) 2020,2023 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
# SPDX-License-Identifier: GPL-3.0-or-later
#

prog		= tempest
bindir		= $(HOME)/.local/bin
sharedir	= $(HOME)/.local/share/tempest
icondir		= $(sharedir)
desktopdir	= $(HOME)/.local/share/applications/
autostartdir	= $(HOME)/.config/autostart/
VERSION		:= $(shell poetry version -s)
WHEEL_FILE	= dist/$(prog)-$(VERSION)-py3-none-any.whl

default:
	@echo "General targets"
	@echo "  init                  Install required packages, setup env"
	@echo "  clean                 Basic file cleanup"
	@echo "  clobber,realclean     Remove all generated files"
	@echo "  install               Installs most files"
	@echo "  autostart             also installs autostart file"
	@echo "  uninstall             Remove installed files"
	@echo "  kill                  Stop the $(prog) program"
	@echo "Developer targets"
	@echo "  flake8                Run flake8 on all code"
	@echo "  tags,etags,TAGS       Build TAGS DB"


check_venv:
	@if test -z $$VIRTUAL_ENV; then \
		echo "A python virtual environment is required"; \
		echo "Suggest using direnv and run 'direnv allow'"; \
		exit 1; \
	fi

# Run this to initialize everything for development
init: check_venv
	poetry install
	pip install -e .

# install and run on rhost in directory $(prog)
#rrun:	rinstall
#	ssh -t $(rhost) 'cd $(prog)/ && DISPLAY=:0 ./$(prog).py || ./$(cleanup)'
#
#
# rmake <host> install
# ssh -t <host> DISPLAY=:0 launch-tempest

build $(WHEEL):
	poetry build


desktop_file = $(prog).desktop
launch_file = launch-tempest
icon_spath = tempest/icons/
icon_file = tempest.png

install: $(icon_spath)/$(icon_file) $(desktop_file)
	-pipx uninstall $(prog)
	pipx install $(WHEEL_FILE)
	install -d $(desktopdir) $(icondir) $(bindir)
	install -m 664 $(icon_spath)/$(icon_file) $(icondir)
	install $(desktop_file) $(desktopdir)
	install $(launch_file) $(bindir)

autostart: install
	install -d $(autostartdir)
	install $(desktop_file) $(autostartdir)

uninstall:
	$(RM) $(bindir)/$(launch_file)
	$(RM) $(icondir)/$(icon_file)
	$(RM) $(desktopdir)/$(desktop_file)
	$(RM) $(autostartdir)/$(desktop_file)
	-rmdir $(icondir)
	pipx uninstall $(prog)

% :: %.in
	sed -e 's%@BINDIR@%$(bindir)%' \
	    -e 's%@SHAREDIR@%$(sharedir)%' \
	    $< > $@


kill:
	kill $(shell cat data/cache/pid)

clean:
	$(RM) .*~
	$(RM) $(desktop_file)
	find * \( -name __pycache__ -o -name '*~' -o -name '*.pyc' \) \
		-print0 | xargs -0 $(RM) -r

clobber realclean: clean
	$(RM) -r data/ dist/ TAGS



flake8:
	flake8 --statistics --show-source --exclude=old tempest/ | less -FRXiM

pyfiles := $(shell find tempest/ -name '*.py')

TAGS:	$(pyfiles)
	etags $(pyfiles)

tags etags: TAGS

.PHONY:	default clean clobber realclean tags etags
.PHONY: init install uninstall autostart kill check_venv
