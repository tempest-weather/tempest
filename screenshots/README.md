# Screenshots for tempest

This is a selection of some of the screens in tempest.


### Main daily weather display
![daily-forecast-screenshot](daily1.jpeg)

---
### Hourly graph
![hourly-forecast-screenshot](hourly.jpeg)

---
### Radar
![radar-screenshot](radar.jpeg)

---
### Alert summary
![info-screenshot](alerts.jpeg)

---
### Miscellaneous information
![info-screenshot](info.jpeg)
