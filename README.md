# Tempest - Display local weather

_Designed with a Raspberry Pi with a 7" 800x480 touchscreen in mind, but will scale to other window sizes_


[![daily-forecast-screenshot](screenshots/daily1.jpeg)](screenshots/README.md)
<br>(click image to see additional screenshots)


## Introduction

Tempest is a weather display program. Its purpose is to
display local weather condtions, including water temperature/level,
radar, and forecasts.  It is designed for a Raspberry PI and makes a
nice little weather kiosk.  Internet connectivity is required to
access data sources.

Due to some of the data sources, this likely will not work for
locations outside the United States.

Although designed for 800x480 screen, is should scale automatically to
other screen sizes.

## Hardware

This was developed for a [Raspberry Pi](https://www.raspberrypi.org/)
[3b Model
B+](https://www.raspberrypi.org/products/raspberry-pi-3-model-b-plus/)
with the official [7" LCD touch
screen](https://www.raspberrypi.org/products/raspberry-pi-touch-display/)
running [Raspbian](https://www.raspberrypi.org/downloads/).  Any
comparable Raspberry Pi will be suficient.  Much of the development
has been done on an [Ubuntu](https://ubuntu.com/) desktop system, so
this should run on most any
[Linux](https://en.wikipedia.org/wiki/Linux)-based system and maybe
even any system with [python3](https://www.python.org/) installed but
you are on your own if you go there.

Some of the directions below assumes you are on a a Raspberry Pi or
command compatible Linux system.


## Install / download

```
$ sudo apt install git make poetry
$ git clone https://gitlab.com/tempest-weather/tempest.git
$ cd tempest
```

## Dependencies

You may need to install additional few packages.  git, make and poetry
are mentioned above, but you may also need the following:

```
$ sudo apt install pipx
$ sudo apt install python3-poetry
$ sudo apt install python3-cairo libcairo2-dev
$ sudo apt install gobject-introspection libgirepository1.0-dev
$ sudo apt install librsvg2-dev
```

## Setup

The first thing you need to do is copy `tempest.conf.sample` to
`tempest.conf` and fill in values for your setup. The main items to
change are your latitude (LAT) & longitude (LON), stations, and radar
sites.  To get your latitude and longitude, some options to are try
include <https://gps-coordinates.org/> and
<https://www.gps-coordinates.net/>.  You can also specify a WATER_SITE.
This will fetch the water height and temperature (if available) at that
location.  The `tempest.conf` file needs to be either in the current
directory where you run tempest ($HOME will work well),
or in `$HOME/.config/tempest/tempest.conf`.
If a config files exits in multiple locations, they will be merged with the
current directory one overriding any duplicate entry from the other location.

To setup the enviroment, you will need to install
dependencies.  Running the following command should do it.

```bash
make init
```

Then create the build and install the files.

```bash
poetry build
make install
```

If you want this program to run at login, run the following.
If you want that to happen automatically at boot, you'll have to make sure
your user automatically logs in at boot (default on Raspian)

```bash
make autostart
```

To undo the actions of `make install` and/or `make autostart`, run

```bash
make uninstall
```

On a [Raspberry Pi](https://www.raspberrypi.org/) with the official
touchscreen the following file
`/sys/class/backlight/rpi_backlight/brightness` should exist.  If it
does and you want this program to be able to change the brightness,
change the udev rules as follows to give it permission to do so.

```bash
echo 'SUBSYSTEM=="backlight" RUN+="/bin/chmod 0666 /sys/class/backlight/%k/brightness /sys/class/backlight/%k/bl_power"' | sudo tee /etc/udev/rules.d/80-backlight.rules > /dev/null
sudo udevadm control --reload-rules
sudo udevadm trigger
```

## Usage

Now you should be able to run `python3 tempest.py` (from the source
directory) or `/path/to/launch-tempest` to start the program.

This program can be used with a keyboard and/or mouse, but it is
mainly intended for use with a touchscreen.

* Touching (mouse click) near to upper right corner will exit the program.
* Touching near the top of the screen (other then the upper right
  corner) will return to the main daily weather display.

On the Daily weather screens

* Touching the current temperature will take you to the radar
* Touching the current conditions (wind, humidity, alert summary) will
  take you to the alert summary screen
* Touching the daily summary will take you to the hourly weather graph
  for that day (if available)

Other screens

* On the hourly weather display or radar screens, touching most
  anywhere will take you to the main screen or the second daily page
* On the alert summary screen touching an alert entry will take you to
  the details for that alert
* On an alert detail screen, touching almost anywhere will take you
  back to the alert summary screen

Swiping left or right will move to the next or previous screen
respectively.  The screen order is: Daily 1, Daily 2, Hourly 1, Hourly
2, Hourly3, Hourly4, Radar, Alert Summary, Information.  Going past
either end will loop around.

Swiping up or down will change the screen brightness (on supported
systems).  The screen can also be automatically dimmed at certain
times of the day where they is no user input for a set amount of time.
See the comments in the config file for details.  The screen will
never automatically dim when there is a weather alert.


While running there are some keyboard shortcuts to switch to various screens

* __d__: Displays the main daily weather weather screen.  If already
  displaying that screen will show the daily weather for the next
  couple of days.  If at the last daily screen, another 'd' will take
  you back to the first.
* __h__: Display hourly data graphs.  Each time h is entered the
  display will advance to the next day.  After the last day, another h
  will return to the first day's hourly graph
* __r__: Display radar
* __i__: Displays an info screen which contains some additiional
  information like sunrise and sunset times.
* __a__: Display alert summary screen.  You can get to the alert
  details by touching (clicking) the alert
* __q__: Quits the program


### Icons

See [icons/README.md](icons/README.md) for information on installing
additional sets of icons.


## Influence, Credit, and History

* This project originated with code written by Jim Kemp and
  published at
  <https://www.instructables.com/id/Raspberry-Pi-Internet-Weather-Station/>.
  It pulled data from weather.com via pywapi.
* A later version, called PiWeatherRock by Gene Liverman, switched to getting
  data from Weather Underground, but that was later changed to Dark Sky.
  <https://github.com/genebean/PiWeatherRock>.  This project was forked from
  this repository at [commit 6352d69](https://github.com/genebean/PiWeatherRock/tree/6352d6980f355b12bea6ee9165b90d7fc872b359)
* Some ideas were taken from
  <https://github.com/sarnold/pitft-weather-display>.
* Some ideas were also taken from <https://code.activestate.com/recipes/577911/>


### Icons

See [icons/default/README.md](icons/default/README.md) for details on where the icons came from.


### Weather data

Hourly forecast, daily forecast, weather alerts and current conditions
are provided by the [National Weather Service
(NWS)](https://www.weather.gov/) via their [API Web
Service](https://www.weather.gov/documentation/services-web-api)


### Water data

Water data is provided by U.S. Geological Survey.<br/>
<https://waterdata.usgs.gov/nwis><br/>
<https://waterdata.usgs.gov/nwis?automated_retrieval_info>


### Radar

Radar images are provided by the National Weather Service<br/>
<https://radar.weather.gov/><br/>
<https://www.weather.gov/jetstream/ridge_download>
